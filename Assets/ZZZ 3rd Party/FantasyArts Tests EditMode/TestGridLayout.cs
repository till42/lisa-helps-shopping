﻿using System.Runtime.CompilerServices;
using NUnit.Framework;
using FA.UI.GridLayout;
using UnityEngine;

namespace FantasyArts.Tests {
public class TestStageProgress {
    private GameObject _parent;
    private GameObject _child1;
    private GameObject _grandChild1;
    private GameObject _grandChild2;
    private GameObject _grandChild3;
    private GameObject _child2;
    private GameObject _grandChild4;
    private GameObject _grandChild5;
    private GameObject _grandChild6;
    private GameObject _child3;
    private GameObject _grandChild7;
    private GameObject _grandChild8;
    private GameObject[] _grandChildren;

    [SetUp]
    public void SetupSampleHierarchy() {
        _parent = new GameObject();


        _child1 = GameObject.Instantiate(_parent, _parent.transform);
        _grandChild1 = GameObject.Instantiate(_parent, _child1.transform);
        _grandChild2 = GameObject.Instantiate(_parent, _child1.transform);
        _grandChild3 = GameObject.Instantiate(_parent, _child1.transform);
        _child2 = GameObject.Instantiate(_parent, _parent.transform);
        _grandChild4 = GameObject.Instantiate(_parent, _child2.transform);
        _grandChild5 = GameObject.Instantiate(_parent, _child2.transform);
        _grandChild6 = GameObject.Instantiate(_parent, _child2.transform);
        _child3 = GameObject.Instantiate(_parent, _parent.transform);
        _grandChild7 = GameObject.Instantiate(_parent, _child3.transform);
        _grandChild8 = GameObject.Instantiate(_parent, _child3.transform);

        _grandChildren = new GameObject[8] {
            GameObject.Instantiate(_parent, _child1.transform),
            GameObject.Instantiate(_parent, _child1.transform),
            GameObject.Instantiate(_parent, _child1.transform),
            GameObject.Instantiate(_parent, _child2.transform),
            GameObject.Instantiate(_parent, _child2.transform),
            GameObject.Instantiate(_parent, _child2.transform),
            GameObject.Instantiate(_parent, _child3.transform),
            GameObject.Instantiate(_parent, _child3.transform)
        };
    }

    [Test]
    public void TestE0_EmptyHierarchyTest_NoParent() {
        // arrange: in setup
        GameObject someObject = new GameObject();

        // act
        int firstChild = someObject.transform.GridFirstChildIndex();

        // assert
        Assert.AreEqual(-1, firstChild);
    }

    [Test]
    public void TestE1_EmptyHierarchyTest_NoChild() {
        // arrange: in setup
        GameObject someObject = new GameObject();
        GameObject someChild = GameObject.Instantiate(someObject, someObject.transform);

        // act
        int firstGrandChild = someChild.transform.GridFirstChildIndex();

        // assert
        Assert.AreEqual(-1, firstGrandChild);
    }

    [Test]
    public void TestS0_SampleHierarchyTest_FirstChild1() {
        // arrange: in setup
        // act
        int firstIndexChild1 = _child1.transform.GridFirstChildIndex();

        // assert
        Assert.AreEqual(0, firstIndexChild1);
    }

    [Test]
    public void TestS1_SampleHierarchyTest_FirstChild3() {
        // arrange: in setup
        // act
        int firstIndexChild3 = _child3.transform.GridFirstChildIndex();

        // assert
        Assert.AreEqual(7, firstIndexChild3);
    }

    [Test]
    public void TestS2_SampleHierarchyTest_GrandChildIndex0() {
        // arrange: in setup
        // act
        int index = _grandChildren[0].transform.GridItemIndex();

        // assert
        Assert.AreEqual(0, index);
    }
}
}