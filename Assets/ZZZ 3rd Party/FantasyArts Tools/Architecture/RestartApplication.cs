﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FA.Architecture {
public class RestartApplication : MonoBehaviour {
    public void DoRestart() {
        SceneManager.LoadScene(sceneBuildIndex: 0, LoadSceneMode.Single);
    }
}
}