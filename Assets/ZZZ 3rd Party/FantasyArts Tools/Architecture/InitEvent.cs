﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace FA.Architecture {
public class InitEvent : MonoBehaviour, ICreateable {
    public enum FA_InitType {
        Created,
        Awake,
        OnEnable,
        Start
    }

    [SerializeField] private FA_InitType _initType;

    [SerializeField] private UnityEvent _onInit;

    public void Created() {
        if (_initType == FA_InitType.Created)
            _onInit?.Invoke();
    }

    private void Awake() {
        if (_initType == FA_InitType.Awake)
            _onInit?.Invoke();
    }

    private void OnEnable() {
        if (_initType == FA_InitType.OnEnable)
            _onInit?.Invoke();
    }

    private void Start() {
        if (_initType == FA_InitType.Start)
            _onInit?.Invoke();
    }
}
}