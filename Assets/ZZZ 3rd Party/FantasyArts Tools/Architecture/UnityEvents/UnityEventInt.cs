﻿using UnityEngine.Events;

namespace FA.Architecture.UnityEvents {
[System.Serializable]
public class UnityEventInt : UnityEvent<int> {
}
}