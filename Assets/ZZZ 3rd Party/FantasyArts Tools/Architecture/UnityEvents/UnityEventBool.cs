﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace FA.Architecture.UnityEvents {
[System.Serializable]
public class UnityEventBool : UnityEvent<bool> {
}
}