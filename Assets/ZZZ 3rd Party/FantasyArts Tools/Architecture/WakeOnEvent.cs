﻿using FA.Architecture.Scriptable;

namespace FA.Architecture {
public class WakeOnEvent : ScriptableEventListener, ICreateable {
    public void Created() {
        OnEnable();
    }

    public override void OnEventRaised() {
        gameObject.SetActive(true);
        base.OnEventRaised();
    }
}
}