﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.Architecture {
public class DisableOnCreated : MonoBehaviour, ICreateable {
    public void Created() {
        gameObject.SetActive(false);
    }
}
}