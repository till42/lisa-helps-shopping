﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.Architecture {
public class SelfDestructAfter : MonoBehaviour {
    [SerializeField] private float _duration = 0.5f;

    public void DoSelfDestruct() {
        StartCoroutine(Co_WaitOnDestruct(_duration));
    }

    private IEnumerator Co_WaitOnDestruct(float duration) {
        yield return new WaitForSeconds(duration);
        Destroy(gameObject);
    }
}
}