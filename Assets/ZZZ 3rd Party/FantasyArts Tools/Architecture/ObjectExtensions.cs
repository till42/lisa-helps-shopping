﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FA.Architecture {
public static class ObjectExtensions {
    /// <summary>
    /// Use this method to get all loaded objects of some type, including inactive objects. 
    /// This is an alternative to Resources.FindObjectsOfTypeAll (returns project assets, including prefabs), and GameObject.FindObjectsOfTypeAll (deprecated).
    /// </summary>
    /// <returns>all active and inactive instances in the scene</returns>
    public static List<T> FindAllObjectInOpenScenes<T>() {
        List<T> results = new List<T>();
        for (int i = 0; i < SceneManager.sceneCount; i++) {
            var scene = SceneManager.GetSceneAt(i);
            // if (s.isLoaded) {
            var allGameObjects = scene.GetRootGameObjects();
            for (int j = 0; j < allGameObjects.Length; j++) {
                var go = allGameObjects[j];
                results.AddRange(go.GetComponentsInChildren<T>(true));
            }

            // }
        }

        return results;
    }

    /// <summary>
    /// Use this method to get all loaded objects of some type, including inactive objects. 
    /// This is an alternative to Resources.FindObjectsOfTypeAll (returns project assets, including prefabs), and GameObject.FindObjectsOfTypeAll (deprecated).
    /// </summary>
    /// <returns>all active and inactive instances in the scene</returns>
    public static List<T> FindAllObjectInScene<T>(Scene scene) {
        List<T> results = new List<T>();
        for (int i = 0; i < SceneManager.sceneCount; i++) {
            var s = SceneManager.GetSceneAt(i);
            // if (s.isLoaded) {
            var allGameObjects = s.GetRootGameObjects();
            for (int j = 0; j < allGameObjects.Length; j++) {
                var go = allGameObjects[j];
                results.AddRange(go.GetComponentsInChildren<T>(true));
            }

            // }
        }

        return results;
    }
}
}