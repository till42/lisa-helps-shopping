﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace FA.Architecture.Scriptable {
public class ScriptableEventListener : MonoBehaviour {
    [SerializeField] private ScriptableEvent scriptableEvent;

    [SerializeField] protected UnityEvent response;

    [SerializeField] protected bool IsDebug = false;

    protected virtual void OnEnable() {
        scriptableEvent.RegisterListener(this);
    }

    protected virtual void OnDisable() {
        scriptableEvent.UnregisterListener(this);
    }

    public virtual void OnEventRaised() {
        if (IsDebug)
            Debug.Log($"{scriptableEvent} raised. Responding on {gameObject}", this);
        response.Invoke();
    }
}
}