﻿using UnityEngine;
using System;

namespace FA.Architecture.Scriptable {
    
    [CreateAssetMenu (menuName = "FantasyArts/ScriptableFloat")]
    public class ScriptableFloat : ScriptablePrimitive {

        [SerializeField] private float value = default;

        public event Action<float> OnValueChanged = delegate { };

        public float Value {
            get => value;
            set {
                this.value = value;
                fireOnValueChanged();
            }
        }

        private void OnValidate()
        {
            fireOnValueChanged();
        }

        public override void Reset() {
            Value = default;
        }

        protected override void fireOnValueChanged()
        {
            OnValueChanged?.Invoke(value);
        }
    }
}