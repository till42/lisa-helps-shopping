﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.Architecture.Scriptable {
    public class EditorOverrideScriptableInt : MonoBehaviour {

        [SerializeField] private ScriptableInt scriptableInt;

        [SerializeField] private int value = 1;

        [SerializeField] private bool isAwake;

        [SerializeField] private bool isOnEnable;

        [SerializeField] private bool isStart;


#if UNITY_EDITOR
        void Awake() {
            if (!isAwake)
                return;

            scriptableInt.Value = value;
        }

        void OnEnable() {
            if (!isOnEnable)
                return;

            scriptableInt.Value = value;
        }

        void Start() {
            if (!isStart)
                return;

            scriptableInt.Value = value;
        }
#endif
    }
}