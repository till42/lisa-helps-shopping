﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.Architecture.Scriptable
{
    public class ScriptableEventRaise : MonoBehaviour
    {
        [SerializeField] private ScriptableEvent scriptableEvent;

        [SerializeField] private bool isAwake;

        [SerializeField] private bool isOnEnable;

        [SerializeField] private bool isStart;
        
        void Awake() {
            if (!isAwake)
                return;

            scriptableEvent.Raise();
        }

        void OnEnable() {
            if (!isOnEnable)
                return;

            scriptableEvent.Raise();
        }

        void Start() {
            if (!isStart)
                return;

            scriptableEvent.Raise();
        }
        
    }
}