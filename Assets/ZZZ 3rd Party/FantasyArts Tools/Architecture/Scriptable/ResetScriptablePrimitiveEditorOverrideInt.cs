﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace FA.Architecture.Scriptable {
    
    public class ResetScriptablePrimitiveEditorOverrideInt : MonoBehaviour {

        [SerializeField] private ScriptableInt scriptableInt;

        [SerializeField] private bool _isAwake;

        [SerializeField] private bool _isOnEnable;

        [SerializeField] private bool _isStart;

        #if UNITY_EDITOR
        [SerializeField] private bool _isEditorOverride;

        [SerializeField, EnableIf("_isEditorOverride")] private int _value;
        #endif
        

        void Awake() {
            if (!_isAwake)
                return;

            Reset();
        }

        void OnEnable() {
            if (!_isOnEnable)
                return;

            Reset();
        }

        void Start() {
            if (!_isStart)
                return;


        }

        private void Reset()
        {
            scriptableInt.Reset();
#if UNITY_EDITOR
            if (_isEditorOverride)
                scriptableInt.Value = _value;
#endif
        }
    }
}