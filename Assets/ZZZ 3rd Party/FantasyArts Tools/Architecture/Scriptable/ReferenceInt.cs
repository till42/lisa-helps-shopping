﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace FA.Architecture.Scriptable {
    [Serializable]
    public class ReferenceInt {

        [SerializeField] private bool _isConstant = true;

        [SerializeField] private int _constantValue;

        [SerializeField] private ScriptableInt _scriptableInt = default;

        public int Value {
            get => _isConstant ? _constantValue : _scriptableInt.Value;
            set {
                if (_isConstant)
                    _constantValue = value;
                else
                    _scriptableInt.Value = value;
            }
        }
    }
}