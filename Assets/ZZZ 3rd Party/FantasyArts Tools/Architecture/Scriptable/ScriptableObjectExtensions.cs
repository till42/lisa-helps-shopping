﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEngine;

namespace FA.Architecture.Scriptable {
public static class ScriptableObjectExtensions {
    /// <summary>
    /// Get all instances of a scriptable object in the project
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static T[] GetAllInstances<T>() where T : ScriptableObject {
        string[]
            guids = AssetDatabase.FindAssets("t:" + typeof(T)
                .Name); //FindAssets uses tags check documentation for more info
        T[] a = new T[guids.Length];
        for (int i = 0; i < guids.Length; i++) //probably could get optimized 
        {
            string path = AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = AssetDatabase.LoadAssetAtPath<T>(path);
        }

        return a;
    }
}
}
#endif