﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.Architecture.Scriptable {
    public abstract class ScriptablePrimitive : ScriptableObject {

        public abstract void Reset();

        protected abstract void fireOnValueChanged();

    }
}