﻿using UnityEngine;
using System;


namespace FA.Architecture.Scriptable {
[CreateAssetMenu(menuName = "FantasyArts/ScriptableBool")]
public class ScriptableBool : ScriptablePrimitive {
    [SerializeField] private bool value = default;

    public event Action<bool> OnValueChanged = delegate { };

    public bool Value {
        get => value;
        set {
            this.value = value;
            fireOnValueChanged();
        }
    }

    private void OnValidate() {
        fireOnValueChanged();
    }

    public override void Reset() {
        Value = default;
    }

    protected override void fireOnValueChanged() {
        OnValueChanged?.Invoke(value);
    }
}
}