﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.Architecture.Scriptable {
    public class ResetScriptablePrimitive : MonoBehaviour {

        [SerializeField] private ScriptablePrimitive scriptablePrimitive;

        [SerializeField] private bool isAwake;

        [SerializeField] private bool isOnEnable;

        [SerializeField] private bool isStart;

        void Awake() {
            if (!isAwake)
                return;

            scriptablePrimitive.Reset();
        }

        void OnEnable() {
            if (!isOnEnable)
                return;

            scriptablePrimitive.Reset();
        }

        void Start() {
            if (!isStart)
                return;

            scriptablePrimitive.Reset();
        }

    }
}