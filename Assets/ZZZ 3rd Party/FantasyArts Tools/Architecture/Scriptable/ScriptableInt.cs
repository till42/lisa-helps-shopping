﻿using UnityEngine;
using System;


namespace FA.Architecture.Scriptable {
    [CreateAssetMenu (menuName = "FantasyArts/ScriptableInt")]
    public class ScriptableInt : ScriptablePrimitive {

        [SerializeField] private int value = default;

        public event Action<int> OnValueChanged = delegate { };

        public int Value {
            get => value;
            set {
                this.value = value;
                OnValueChanged?.Invoke(this.value);
            }
        }

        public override void Reset() {
            Value = default;
        }
        
        protected override void fireOnValueChanged()
        {
            OnValueChanged?.Invoke(value);
        }
    }
}