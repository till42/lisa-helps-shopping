﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace FA.Architecture.Scriptable {
    [Serializable]
    public class ReferenceFloat {

        [SerializeField] private bool isConstant = true;

        [SerializeField] private float constantValue;

        [SerializeField] private ScriptableFloat scriptableInt = default;

        public float Value {
            get => isConstant ? constantValue : scriptableInt.Value;
            set {
                if (isConstant)
                    constantValue = value;
                else
                    scriptableInt.Value = value;
            }
        }
    }
}