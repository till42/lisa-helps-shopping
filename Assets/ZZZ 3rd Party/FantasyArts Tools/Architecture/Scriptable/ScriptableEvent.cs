﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEngine;
using NaughtyAttributes;

namespace FA.Architecture.Scriptable {
[CreateAssetMenu(menuName = "FantasyArts/ScriptableEvent")]
public class ScriptableEvent : ScriptableObject {
    private List<ScriptableEventListener> _listeners = new List<ScriptableEventListener>();

    [SerializeField] private bool _isDebug = false;

    [Button]
    public void Raise() {
        if (_isDebug)
            Debug.Log($"{name} is raised", this);

        if (_listeners == null || _listeners.Count <= 0)
            return;
        ;
        for (int i = _listeners.Count - 1; i >= 0; i--) {
            if (_listeners[i] == null)
                continue;
            _listeners[i].OnEventRaised();
        }
    }

    public void RegisterListener(ScriptableEventListener listener) {
        if (_listeners.Contains(listener))
            return;

        _listeners.Add(listener);
    }

    public void UnregisterListener(ScriptableEventListener listener) {
        if (!_listeners.Contains(listener))
            return;

        _listeners.Remove(listener);
    }
}
}