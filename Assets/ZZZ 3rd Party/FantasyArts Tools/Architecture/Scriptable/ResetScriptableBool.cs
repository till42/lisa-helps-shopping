﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.Architecture.Scriptable {
public class ResetScriptableBool : MonoBehaviour {
    [SerializeField] private ScriptableBool _scriptableBool;

    [SerializeField] private bool _isAwake;

    [SerializeField] private bool _isOnEnable;

    [SerializeField] private bool _isStart;

    [SerializeField] private bool _initialValue;

    void Awake() {
        if (!_isAwake)
            return;

        _scriptableBool.Reset();
        _scriptableBool.Value = _initialValue;
    }

    void OnEnable() {
        if (!_isOnEnable)
            return;

        _scriptableBool.Reset();
        _scriptableBool.Value = _initialValue;
    }

    void Start() {
        if (!_isStart)
            return;

        _scriptableBool.Reset();
        _scriptableBool.Value = _initialValue;
    }
}
}