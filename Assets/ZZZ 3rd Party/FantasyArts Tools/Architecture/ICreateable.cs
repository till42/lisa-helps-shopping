﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.Architecture {
public interface ICreateable {
    void Created();
}
}