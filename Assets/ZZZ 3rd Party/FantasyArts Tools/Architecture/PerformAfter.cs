﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace FA.Architecture {
public class PerformAfter : MonoBehaviour {
    [SerializeField] private UnityEvent _onEnable;

    [SerializeField] private float _duration;

    [SerializeField] private UnityEvent _onWaitCompleted;


    private void OnEnable() {
        _onEnable?.Invoke();
        StartCoroutine(Co_WaitAndInvoke());
    }

    private IEnumerator Co_WaitAndInvoke() {
        yield return new WaitForSeconds(_duration);
        _onWaitCompleted?.Invoke();
    }
}
}