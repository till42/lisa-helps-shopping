﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using static FA.Architecture.ObjectExtensions;

namespace FA.Architecture {
public static class CallCreatedMethods {
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    public static void BeforeSceneLoad() {
        SceneManager.sceneLoaded -= OnSceneLoaded;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }


    private static void OnSceneLoaded(Scene scene, LoadSceneMode mode) =>
        FindAllObjectInScene<ICreateable>(scene).ForEach(i => i.Created());
}
}