﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Image = UnityEngine.UI.Image;

namespace FA.Animation {
public class LoopFlash : MonoBehaviour {
    [SerializeField] private float _durationOn = 0.5f;

    [SerializeField] private float _durationOff = 0.2f;

    private Renderer _renderer;
    private Image _image;

    private void OnEnable() {
        _renderer = GetComponent<Renderer>();
        _image = GetComponent<Image>();
        StartCoroutine(Co_Flash(_durationOn, _durationOff));
    }

    private IEnumerator Co_Flash(float durationOn, float durationOff) {
        var waitOn = new WaitForSeconds(durationOn);
        var waitOff = new WaitForSeconds(durationOff);

        while (true) {
            Flash_On();
            yield return waitOn;

            Flash_Off();
            yield return waitOff;

            yield return null;
        }
    }

    private void Flash_On() {
        if (_renderer != null)
            _renderer.enabled = true;
        if (_image != null)
            _image.enabled = true;
    }

    private void Flash_Off() {
        if (_renderer != null)
            _renderer.enabled = false;
        if (_image != null)
            _image.enabled = false;
    }

    private void OnDisable() {
        End();
    }

    public void End() {
        StopAllCoroutines();
        Flash_On();
    }
}
}