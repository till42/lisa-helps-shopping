﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using NaughtyAttributes;


namespace FA.Animation {
    public class LoopScale : MonoBehaviour {

        [SerializeField] private Vector3 alternateScale = new Vector3(1.2f, 1.2f, 1.2f);

        [SerializeField] private float duration = 0.5f;

        [SerializeField] private bool isAnimatingSelf = true;

        [DisableIf("_isAnimatingSelf")]
        [SerializeField] private Transform target;

        private Vector3 _defaultScale;

        void Awake() {
            if (isAnimatingSelf || target == null)
                target = transform;
            _defaultScale = target.localScale;
        }

        void OnEnable() {

            target.DOScale(alternateScale, duration).SetLoops(-1, LoopType.Yoyo);
        }

        void OnDisable() {
            target.DOKill();
            target.localScale = _defaultScale;
        }
    }
}