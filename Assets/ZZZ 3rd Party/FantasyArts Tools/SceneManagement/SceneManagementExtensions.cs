﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace FA.SceneManagement
{
    public static class SceneManagementExtensions
    {
        /// <summary>
        /// go through all loaded scenes and check if the given scene is already loaded
        /// </summary>
        /// <param name="sceneName">name of the scene to be checked</param>
        /// <returns>true: scene is already loaded. false: scene is not loaded</returns>
        public static bool IsLoaded(string sceneName)
        {
            for (var i = 0; i < SceneManager.sceneCount; i++)
            {
                var loadedScene = SceneManager.GetSceneAt(i);
                if (loadedScene.name == sceneName)
                    return true;
            }

            return false;
        }
        
        /// <summary>
        /// go through all loaded scenes and check if the given scene is already loaded
        /// </summary>
        /// <param name="scene">scene to be checked</param>
        /// <returns>true: scene is already loaded. false: scene is not loaded</returns>
        public static bool IsLoaded(Scene scene)
        {
            for (var i = 0; i < SceneManager.sceneCount; i++)
            {
                var loadedScene = SceneManager.GetSceneAt(i);
                if (loadedScene.buildIndex == scene.buildIndex)
                    return true;
            }

            return false;
        }
    }
}