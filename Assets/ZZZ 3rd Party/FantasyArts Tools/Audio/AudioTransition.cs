﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace FA.Audio {
public class AudioTransition : MonoBehaviour {
    [SerializeField] private AudioMixerSnapshot _snapshot;

    [SerializeField] private float _snapshotDuration = 0.1f;

    public void DoTransition() {
        if (_snapshot != null)
            _snapshot.TransitionTo(_snapshotDuration);
    }
}
}