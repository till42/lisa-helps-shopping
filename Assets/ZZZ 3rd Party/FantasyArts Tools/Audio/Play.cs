﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Audio;

namespace FA.Audio {
public class Play : MonoBehaviour {
    [SerializeField] private bool _isPlayOnEnable = true;

    [SerializeField] private UnityEvent _onPlayed;

    [SerializeField] private AudioMixerSnapshot _snapshot;

    [SerializeField] private float _snapshotDuration = 0.1f;

    private AudioSource _audioSource;


    private void OnEnable() {
        _audioSource = GetComponent<AudioSource>();
        if (_isPlayOnEnable)
            DoPlay();
    }

    public void DoPlay() {
        if (_snapshot != null)
            _snapshot.TransitionTo(_snapshotDuration);
        StartCoroutine(Co_PlayAudioClip());
    }

    private IEnumerator Co_PlayAudioClip() {
        if (_audioSource == null) {
            Debug.LogWarning($"no audiosource found on {gameObject.name}");
            yield break;
        }

        _audioSource.Play();
        yield return new WaitForSeconds(_audioSource.clip.length);
        _onPlayed?.Invoke();
    }
}
}