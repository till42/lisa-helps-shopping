﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Runtime.InteropServices;
using FA.JavaScriptInteraction;

namespace FA.KongAPI {
public class KongregateAPI : MonoBehaviour {
    public string Username {
        get => _username;
        private set {
            _username = value;
            OnUpdateUsername?.Invoke(_username);
        }
    }

    private string _username { get; set; } = "not logged in";

    public event Action<string> OnUpdateUsername = delegate { };

    private Domain _domain;

    private static KongregateAPI instance;

    private void Awake() {
        _domain = GetComponent<Domain>();
        if (_domain == null)
            _domain = FindObjectOfType<Domain>();
        instance = this;
    }


    void Start() {
        gameObject.name = "KongregateAPI";

        //check if we can find a helper to read the url
        if (_domain != null && !_domain.DomainType.IsKong()) {
            Debug.Log(
                $"Found {_domain.URL} is not detected as kongregate,but instead {_domain.DomainTypeString}");
            return; //get outta here!
        }

        Debug.Log($"Calling KongInitAPI() in JavaLib");
        KongInitAPI();
#if UNITY_WEBGL && UNITY_EDITOR
        OnKongregateAPILoaded("test data from unity");
#endif
    }

#if UNITY_WEBGL && !UNITY_EDITOR
        [DllImport("__Internal")]
        public static extern void KongInitAPI();
#else
    /// <summary>
    /// Sync the filesystem within HTML5. Only required in WebGL build
    /// </summary>
    /// <returns>0, if succesfull. Err code otherwise</returns>
    public static void KongInitAPI() {
    }
#endif

#if UNITY_WEBGL && !UNITY_EDITOR
        [DllImport("__Internal")]
        public static extern bool GetKongIsGuest();
#else
    /// <summary>
    /// Sync the filesystem within HTML5. Only required in WebGL build
    /// </summary>
    /// <returns>0, if succesfull. Err code otherwise</returns>
    public static bool GetKongIsGuest() => true;

#endif

    public void OnKongregateAPILoaded(string userInfoString) {
        //OnKongregateUserInfo(userInfoString);
        Debug.Log("Kong has answered");
        Debug.Log(convertString(userInfoString));

        Debug.Log($"user is guest on kong={GetKongIsGuest()}");

        Stat("game_start", 1);
    }

    public string convertString(string userInfoString) {
        //we don't accept empty strings
        if (string.IsNullOrEmpty(userInfoString))
            return "";

        //split the string
        var info = userInfoString.Split('|');

        //only one value --> get out of here
        if (info.Length <= 1)
            return "";

        var userId = System.Convert.ToInt32(info[0]);
        Username = info[1];
        var gameAuthToken = info[2];
        return $"userId={userId} username={Username} gameAuthToken={gameAuthToken}";
    }


    public void Stat(string statName, int value) {
        SubmitStat(statName, value);
    }

    public void Gold(int value) {
        SubmitStat("gold", value);
    }

    public void Highscore(int value) {
        SubmitStat("highscore", value);
    }

    public void SubmitRoundsPlayed(int value) {
        SubmitStat("rounds_played", value);
    }

    public void CheckRegistration() {
        if (GetKongIsGuest())
            return;

        //not a kong guest! Quickly register
    }

#if UNITY_WEBGL && !UNITY_EDITOR
        [DllImport("__Internal")]
        public static extern void SubmitStat(string statName, int value);
#else
    /// <summary>
    /// Submit a stat to Kong API
    /// </summary>
    public static void SubmitStat(string statName, int value) {
        Debug.Log($"KongAPI: SubmitStat {statName} with value={value}");
    }
#endif

    public void Register() {
        AddSignInEventListener();
        ShowSignInBox();
    }

#if UNITY_WEBGL && !UNITY_EDITOR
        [DllImport("__Internal")]
        public static extern void ShowSignInBox();
#else
    /// <summary>
    /// Submit a stat to Kong API
    /// </summary>
    public static void ShowSignInBox() {
        Debug.Log($"ShowSignInBox");
    }
#endif

#if UNITY_WEBGL && !UNITY_EDITOR
        [DllImport("__Internal")]
        public static extern void ShowRegistrationBox();
#else
    /// <summary>
    /// Submit a stat to Kong API
    /// </summary>
    public static void ShowRegistrationBox() {
        Debug.Log($"ShowRegistrationBox");
    }
#endif


    /// <summary>
    /// Called when the Kongregate user signs in, parse the tokenized user-info string that we
    // generate below using Javascript.
    /// </summary>
    /// <param name="userInfoString"></param>
    public void OnKongregateUserSignedIn(string userInfoString) {
        Debug.Log($"OnKongregateUserSignedIn" + convertString(userInfoString));
    }


#if UNITY_WEBGL && !UNITY_EDITOR
        [DllImport("__Internal")]
        private static extern void AddSignInEventListener();
#else
    /// <summary>
    /// Listen to the sign in event
    /// </summary>
    private static void AddSignInEventListener() {
        instance.OnKongregateUserSignedIn("1231354|unsername|gameAuthToken");
    }
#endif

    //    public Text UserNameTextComponent;
    //    private static KongregateAPI instance;

    //    private static string userName = "you!";
    //    public static string UserName { get { return userName; } }

    //    public void Start() {
    //        if (instance == null) {
    //            instance = this;
    //        } else if (instance != this) {
    //            Destroy(gameObject);
    //            return;
    //        }

    //        DontDestroyOnLoad(gameObject);
    //        gameObject.name = "KongregateAPI";

    //#pragma warning disable CS0618
    //        Application.ExternalEval(
    //          @"if(typeof(kongregateUnitySupport) != 'undefined'){
    //        kongregateUnitySupport.initAPI('KongregateAPI', 'OnKongregateAPILoaded');
    //      };"
    //        );
    //    }
    //#pragma warning restore CS0618

    //    public void OnKongregateAPILoaded(string userInfoString) {
    //        OnKongregateUserInfo(userInfoString);
    //    }

    //    public void OnKongregateUserInfo(string userInfoString) {
    //        var info = userInfoString.Split('|');
    //        var userId = System.Convert.ToInt32(info[0]);
    //        var username = info[1];
    //        var gameAuthToken = info[2];
    //        Debug.Log("Kongregate User Info: " + username + ", userId: " + userId);

    //        userName = username;

    //        if (UserNameTextComponent != null)
    //            UserNameTextComponent.text = UserName;
    //    }

    //    public static void SendKongScore(int value) {
    //        Debug.LogFormat("Sent to Kong: highscore={0}, user={1}", value, userName);
    //        if (userName == "you!")
    //            return;

    //#pragma warning disable CS0618
    //        Application.ExternalCall("kongregate.stats.submit", "highscore", value);
    //    }
    //#pragma warning restore CS0618
}
}