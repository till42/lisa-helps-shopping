﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace FA.KongAPI {
public class UpdateKongUser : MonoBehaviour {
    private TMP_Text _tmpText;
    private KongregateAPI _kongregateApi;

    private void OnEnable() {
        _tmpText = GetComponent<TMP_Text>();

        _kongregateApi = FindObjectOfType<KongregateAPI>();
        if (_kongregateApi == null) {
            _tmpText.text = "API not found";
            return;
        }

        _kongregateApi.OnUpdateUsername += HandleUpdateUsername;
        HandleUpdateUsername(_kongregateApi.Username);
    }

    private void HandleUpdateUsername(string username) {
        _tmpText.text = username;
    }

    private void OnDisable() {
        _kongregateApi.OnUpdateUsername -= HandleUpdateUsername;
    }
}
}