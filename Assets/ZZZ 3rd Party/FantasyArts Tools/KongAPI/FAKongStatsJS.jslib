var FAKongStatsJS = {
  $ClassVar: {},

  KongInitAPI: function() {
    console.log("KongInitAPI : function start");

    if (typeof kongregateUnitySupport != "undefined") {
      console.log("kongregateUnitySupport is defined");
      try {
        console.log(
          "kongregateUnitySupport.loaded: " + kongregateUnitySupport.loaded
        );
        kongregateUnitySupport.initAPI(
          "KongregateAPI",
          "OnKongregateAPILoaded"
        );
      } catch (error) {
        console.log("KongInitAPI caught error" + error);
      }
    } else {
      console.log("kongregateUnitySupport is undefined");
    }
  },

  SubmitStat: function(_statName, val) {
    try {
      // parent.kongregate.stats.submit("win_match", 1);
      // parent.kongregate.stats.submit("match_started", 1);

      var statName = Pointer_stringify(_statName);

      console.log("SubmitStat : function (" + statName + ", " + val + ")");

      kongregate.stats.submit(statName, val);
    } catch (error) {
      console.log("SubmitStat caught error" + error);
    }
  },

  GetKongIsGuest: function() {
    var isGuest = kongregate.services.isGuest();
    return isGuest;
  },

  GetKongUserId: function() {
    var userId = parent.kongregate.services.getUserId();
    console.log("GetKongUserId. UserId is: " + userId);
    return userId;
  },

  GetKongUsername: function() {
    var returnStr = parent.kongregate.services.getUsername();
    console.log("GetKongUsername. Username is: " + returnStr);
    var buffer = _malloc(lengthBytesUTF8(returnStr) + 1);
    writeStringToMemory(returnStr, buffer);
    return buffer;
  },

  GetKongAuthToken: function() {
    var returnStr = parent.kongregate.services.getGameAuthToken();
    console.log("GetKongAuthToken. AuthToken is: " + returnStr);
    var buffer = _malloc(lengthBytesUTF8(returnStr) + 1);
    writeStringToMemory(returnStr, buffer);
    return buffer;
  },

  ShowRegistrationBox: function() {
    try {
      console.log("ShowRegistrationBox called");
      kongregate.services.showRegistrationBox();
    } catch (error) {
      console.log("ShowRegistrationBox caught error" + error);
    }
  },

  ShowSignInBox: function() {
    try {
      console.log("ShowSignInBox called");
      kongregate.services.showSignInBox();
    } catch (error) {
      console.log("ShowSignInBox caught error" + error);
    }
  },

  AddSignInEventListener: function() {
    try {
      console.log("AddSignInEventListener called");

      kongregate.services.addEventListener("login", function() {
        var services = kongregate.services;
        var params = [
          services.getUserId(),
          services.getUsername(),
          services.getGameAuthToken()
        ].join("|");
        kongregateUnitySupport
          .getUnityObject()
          .SendMessage("KongregateAPI", "OnKongregateUserSignedIn", params);
      });
    } catch (error) {
      console.log("AddSignInEventListener caught error" + error);
    }
  }
};

autoAddDeps(FAKongStatsJS, "$ClassVar");
mergeInto(LibraryManager.library, FAKongStatsJS);