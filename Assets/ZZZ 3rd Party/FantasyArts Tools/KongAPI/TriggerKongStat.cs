﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace FA.KongAPI {
public class TriggerKongStat : MonoBehaviour {
    [SerializeField] private string _statName;

    private KongregateAPI _kongregateAPI;

    protected virtual void Awake() {
        _kongregateAPI = FindObjectOfType<KongregateAPI>();
    }

    public void DoTrigger(int value) {
        if (string.IsNullOrEmpty(_statName))
            Debug.LogWarning($"Did not submit stat as statname is emptry. Could not sent value={value.ToString()}",
                context: this);

        if (_kongregateAPI == null)
            Debug.LogWarning(
                $"Could not find Kongregate API. Did not submit stat={_statName}, value={value.ToString()}",
                context: this);

        _kongregateAPI.Stat(_statName, value);
    }

    public void DoTrigger(string statName, int value) {
        if (enabled == false)
            Debug.LogWarning(
                $"Component {this} is not enabled. Did not submit stat={statName}, value={value.ToString()}");

        if (_kongregateAPI == null)
            Debug.LogWarning(
                $"Could not find Kongregate API. Did not submit stat={statName}, value={value.ToString()}");

        Debug.Log($"submitting to KongAPI: {statName}={value}");
        _kongregateAPI.Stat(statName, value);
    }
}
}