﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace FA.UI {
/// <summary>
/// loop a scrollable content by moving the y-position of the rectTransform
/// Expected: parent has the mask, pivot is y=0, anchor is set to bottom middle of the parent, so that y=0 means no more content is visible
/// </summary>
public class LoopCreditsScrollContent : MonoBehaviour {
    [SerializeField] private float _speed = 0.2f;

    private RectTransform _rectTransform;

    private RectTransform _parentRectTransform;
    private float _parentHeight;

    private void OnEnable() {
        Initialize();
    }

    private void Start() {
        StartCoroutine(Co_StartLoop());
    }

    private IEnumerator Co_StartLoop() {
        yield return new WaitForEndOfFrame();

        //_rectTransform.DO(0f, _speed).SetEase(Ease.Linear).SetLoops(-1, LoopType.Restart);
        while (true) {
            SetStartPosition();
            float y = _rectTransform.anchoredPosition.y;

            while (y < 0) {
                y += _speed;
                var position = _rectTransform.anchoredPosition;
                position.y = y;
                _rectTransform.anchoredPosition = position;
                yield return null;
            }

            yield return null; //just to make sure not to create an unbreakable endless loop
        }
    }


    private void Initialize() {
        _rectTransform = GetComponent<RectTransform>();
        var parent = transform.parent;
        if (parent == null) {
            Debug.LogWarning("could not find a parent as expected", this);
            return;
        }

        _parentRectTransform = parent.GetComponent<RectTransform>();
        if (_parentRectTransform == null) {
            Debug.LogWarning("could not find a rectTransform in parent as expected", this);
            return;
        }
    }

    private void SetStartPosition() {
        //get own height
        Vector3 position = _rectTransform.anchoredPosition;
        position.y = -_rectTransform.rect.height;

        //get parent height
        position.y -= _parentRectTransform.rect.height;

        //apply
        _rectTransform.anchoredPosition = position;
    }
}
}