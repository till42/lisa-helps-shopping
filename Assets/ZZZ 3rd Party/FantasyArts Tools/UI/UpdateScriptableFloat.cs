﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FA.Architecture.Scriptable;
using FA.Architecture.UnityEvents;
using UnityEngine.Events;

namespace FA.UI {
public class UpdateScriptableFloat : MonoBehaviour {
    [SerializeField] protected ScriptableFloat _scriptableFloat = default;

    [SerializeField] protected UnityEventFloat _onUpdate;

    protected void OnEnable() {
        Validation();

        _scriptableFloat.OnValueChanged += HandleValueChanged;

        //update with default
        HandleValueChanged(_scriptableFloat.Value);
    }

    protected virtual void Validation() {
        if (_scriptableFloat != null) return;

        Debug.LogWarning($"no scriptable int found. creating an instance as dummy");
        _scriptableFloat = new ScriptableFloat();
    }

    protected virtual void HandleValueChanged(float value) {
        _onUpdate?.Invoke(value);
    }

    protected virtual void OnDisable() {
        _scriptableFloat.OnValueChanged -= HandleValueChanged;
    }
}
}