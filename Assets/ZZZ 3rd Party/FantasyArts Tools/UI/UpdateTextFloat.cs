﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using FA.Architecture.Scriptable;
using UnityEngine;


namespace FA.UI {
    public class UpdateTextFloat : UpdateTextAbstract {

        [SerializeField] private ScriptableFloat scriptableFloat = default;

        [SerializeField] protected string formatter = "0.00";

        protected override void OnEnable() {
            base.OnEnable();

            Validation();

            scriptableFloat.OnValueChanged += HandleValueChanged;

            //update with default
            HandleValueChanged(scriptableFloat.Value);
        }

        protected virtual void Validation() {
            if (scriptableFloat == null) {
                Debug.LogWarning($"no scriptable float found. creating an instance as dummy");
                scriptableFloat = new ScriptableFloat();
            }
        }

        protected virtual void HandleValueChanged(float value) {
            handleTextUpdate(value.ToString(formatter));
        }

        protected virtual void OnDisable() {
            scriptableFloat.OnValueChanged -= HandleValueChanged;
        }

    }
}