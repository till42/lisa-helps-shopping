﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FA.Architecture.Scriptable;

namespace FA.UI {
    public class UpdateTextInt : UpdateTextAbstract {

        [SerializeField] private ScriptableInt _scriptableInt = default;

        protected override void OnEnable() {
            base.OnEnable();

            Validation();

            _scriptableInt.OnValueChanged += handleValueChanged;

            //update with default
            handleValueChanged(_scriptableInt.Value);
        }

        protected virtual void Validation() {
            if (_scriptableInt == null) {
                Debug.LogWarning($"no scriptable int found. creating an instance as dummy");
                _scriptableInt = new ScriptableInt();
            }
        }

        protected virtual void handleValueChanged(int value) {
            handleTextUpdate(value.ToString());
        }

        protected virtual void OnDisable() {
            _scriptableInt.OnValueChanged -= handleValueChanged;
        }

    }
}