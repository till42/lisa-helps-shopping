﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace FA.UI
{
    public class OnEnableGrabFocus : MonoBehaviour
    {
        private void OnEnable()
        {
            EventSystem eventSystem = EventSystem.current;
            
            eventSystem.SetSelectedGameObject(gameObject);
        }
    }
}