﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FA.Architecture;

namespace FA.UI {
    public class UpdateTextIntWithinString : UpdateTextInt {

        private const string pattern = "{0}";

        protected override void Validation() {
            base.Validation();

            //validate _default text for the pattern
            if (string.IsNullOrEmpty(_defaultText)) {
                Debug.LogWarning($"_defaultText is not filled. Replacing it with pattern {pattern}");
                _defaultText = pattern;
            }
            else if (!_defaultText.Contains(pattern)) {
                Debug.LogWarning($"_defaultText does not contain the pattern {pattern}. Appended pattern to end of the string");
                _defaultText = _defaultText + pattern;
            }
        }

        protected override void handleValueChanged(int value) {
            handleTextUpdate(_defaultText.Replace(pattern, value.ToString()));
        }

    }
}