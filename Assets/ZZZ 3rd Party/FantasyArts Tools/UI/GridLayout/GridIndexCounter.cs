﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.UI.GridLayout {
public class GridIndexCounter : MonoBehaviour {
    public int Index {
        get {
            if (_indexBuffer == InitialValue)
                //initialize
                _indexBuffer = transform.GridItemIndex();

            return _indexBuffer;
        }
    }

    private int _indexBuffer = InitialValue;
    private const int InitialValue = int.MinValue;

    private void Awake() {
        name += $"[{Index}]";
    }
}
}