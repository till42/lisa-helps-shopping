﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FA.UI.GridLayout {
public static class TransformExtensions {
    /// <summary>
    /// assuming that the transform is a row element in the hierarchy, this function will return the index of the first child
    /// example hierarchy:
    /// grid parent
    ///   + sibling (3 children) [0..2]
    ///   + sibling (2 children) [3..4]
    ///   + transform
    ///   + sibling (4 children)
    ///  --> function will return 5
    /// </summary>
    /// <param name="t">transform of the row in the hierarchy</param>
    /// <returns>index of the first child of this row</returns>
    public static int GridFirstChildIndex(this Transform t) {
        //if t has no parent...
        if (t.parent == null)
            return 0; //step out here with 0

        //if t has no children...
        if (t.childCount <= 0)
            return 0; //step out here with 0

        int index = 0;
        //go through each prev sibling to add up the children
        for (int i = 0; i <= t.GetSiblingIndex() - 1; i++) {
            var siblingTransform = t.parent.GetChild(i);
            index += siblingTransform.childCount;
        }

        return index;
    }

    public static int GridItemIndex(this Transform t) {
        return t.parent.GridFirstChildIndex() + t.GetSiblingIndex();
    }
}
}