﻿using System;
using System.Collections;
using System.Collections.Generic;
using FA.UI.GridLayout;
using UnityEngine;

namespace FA.UI.GridLayout {
public class GridRowCounter : MonoBehaviour {
    private void Awake() {
        var minIndex = transform.GridFirstChildIndex();

        //add own childCount for maxIndex
        var maxIndex = minIndex + transform.childCount - 1;

        name += $" [{minIndex}..{maxIndex}]";
    }
}
}