﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FA.Architecture.Scriptable;
using FA.Architecture.UnityEvents;
using UnityEngine.Events;

namespace FA.UI {
public class UpdateScriptableBool : MonoBehaviour {
    [SerializeField] private ScriptableBool _scriptableBool = default;

    [SerializeField] private UnityEventBool _onUpdate;

    protected void OnEnable() {
        Validation();

        _scriptableBool.OnValueChanged += handleValueChanged;

        //update with default
        handleValueChanged(_scriptableBool.Value);
    }

    protected virtual void Validation() {
        if (_scriptableBool == null) {
            Debug.LogWarning($"no scriptable int found. creating an instance as dummy");
            _scriptableBool = new ScriptableBool();
        }
    }

    protected virtual void handleValueChanged(bool value) {
        _onUpdate?.Invoke(value);
    }

    protected virtual void OnDisable() {
        _scriptableBool.OnValueChanged -= handleValueChanged;
    }
}
}