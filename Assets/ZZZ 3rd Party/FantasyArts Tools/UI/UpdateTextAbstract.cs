﻿using UnityEngine;
using TMPro;

namespace FA.UI {
    /// <summary>
    /// update a text mesh pro with information
    /// </summary>
    public abstract class UpdateTextAbstract : MonoBehaviour {

        //reference to the text mesh pro component
        private TMP_Text _tmpText;

        //buffer for the original default text
        protected string _defaultText = null;

        protected virtual void OnEnable() {
            //get the tmp component
            _tmpText = GetComponent<TMP_Text>();

            //log warning and create a tmp component if not found
            if (_tmpText == null) {
                Debug.LogWarning("Did not find expected text mesh pro component on this game object. Created a dummy component instead");
                _tmpText = gameObject.AddComponent<TMP_Text>();
            }

            //save the default text, if not yet done
            if (_defaultText == null)
                _defaultText = _tmpText.text;
        }

        /// <summary>
        /// to be called from inherited class to perform the update
        /// </summary>
        /// <param name="text">string to put into the text</param>
        protected void handleTextUpdate(string text) {
            _tmpText.text = text;
        }

    }
}