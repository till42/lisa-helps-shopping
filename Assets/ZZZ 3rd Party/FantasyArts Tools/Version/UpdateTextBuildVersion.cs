﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace FA.Version {
public class UpdateTextBuildVersion : MonoBehaviour {
    private TMP_Text _tmpText;

    private void OnEnable() {
        _tmpText = GetComponent<TMP_Text>();
        _tmpText.text = "V" + Application.version;
    }
}
}