﻿#if KONG
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using System.Diagnostics;
using UnityEngine;
using System;
using FA_Tools;

class FA_KongPreBuild : IPreprocessBuildWithReport {

    private const string firefoxPath = @"C:\Program Files\Mozilla Firefox\firefox.exe";
    private const string uploadSite = @"https://www.kongregate.com/games/till42/dungeon-crawler/version/new";

    public int callbackOrder { get { return 0; } }


    public void OnPreprocessBuild(BuildReport report) {

        UnityEngine.Debug.Log("MyCustomBuildProcessor.OnPreprocessBuild for target " + report.summary.platform + " at path " + report.summary.outputPath);

        //only for WebGL Build
        if (report.summary.platform != BuildTarget.WebGL)
            return;

        //set the version time
        FA_Tools.Version.SetDateAndTime();

        //open firefox with the upload page
        Process FireFoxProcess = new Process();
        FireFoxProcess.StartInfo.FileName = firefoxPath;
        FireFoxProcess.StartInfo.Arguments = "-new-tab " + uploadSite;
        FireFoxProcess.Start();
    }
}
#endif
#endif