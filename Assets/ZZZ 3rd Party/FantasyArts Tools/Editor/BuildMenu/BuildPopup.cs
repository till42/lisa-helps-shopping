﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Reflection;

namespace FA.Editor.Build {
#if UNITY_EDITOR


    public class BuildPopup : EditorWindow {
        /// <summary>
        /// start the game with the alternative levels and default hero
        /// </summary>
        [MenuItem("FantasyArts/Build WebGL...")]
        static void Build() {
            //display the popup for the user to set version and path
            BuildPopup.Display();
        }

        private static string currentVersion;
        private static string buildVersion;

        private static string outputPath;
        private static string rootPath;

        private const string PathKey = "BUILD_DEFAULTPATH";

        public static void Display() {
            //get the current version from the prefab
            currentVersion = buildVersion = Version.get;

            //get the path from previous
            rootPath = getRootPath();

            BuildPopup window = CreateInstance<BuildPopup>();
            window.position = new Rect(Screen.width / 2, Screen.height / 2, 550, 450);
            window.titleContent = new GUIContent("Build WebGL");
            window.showModal();
        }

        void showModal() {
            MethodInfo dynShowModal = this.GetType().GetMethod("ShowModal", BindingFlags.NonPublic | BindingFlags.Instance);
            dynShowModal.Invoke(this, new object[] { });
        }

        void OnGUI() {
            GUILayout.Space(10);
            EditorGUILayout.LabelField("Current Version:", currentVersion, EditorStyles.wordWrappedLabel);
            GUILayout.Space(10);
            buildVersion = EditorGUILayout.TextField("Version: ", buildVersion);
            GUILayout.Space(60);
            outputPath = $"{rootPath}{Path.DirectorySeparatorChar}v{buildVersion}";
            EditorGUILayout.LabelField("Output Path:", outputPath, EditorStyles.wordWrappedLabel);
            if (GUILayout.Button("Change output root")) {
                string newRoot = EditorUtility.OpenFolderPanel("Select Build Output Root Path", rootPath, "");
                newRoot = unifyDirectorySeparatorChars(newRoot);
                if (!string.IsNullOrEmpty(newRoot))
                    rootPath = newRoot;
            }
            GUILayout.Space(100);
            EditorGUILayout.BeginHorizontal();
            if (GUILayout.Button("Cancel")) {
                Close();
            }
            if (GUILayout.Button("BUILD")) {
                EditorGUILayout.EndHorizontal();
                //write version, date and time into the version prefab
                if (buildVersion != "")
                    Version.Set(buildVersion);

                //save the root path for next time
                setRootPath(rootPath);

                //close the window
                Close();

                //perform the build
                performBuild();
                return;
            }
            EditorGUILayout.EndHorizontal();
        }

        private static string unifyDirectorySeparatorChars(string path) {
            path = path.Replace('\\', Path.DirectorySeparatorChar);
            path = path.Replace('/', Path.DirectorySeparatorChar);
            return path;
        }

        private void performBuild() {
            var levels = EditorBuildSettings.scenes;
            BuildPipeline.BuildPlayer(levels, outputPath, BuildTarget.WebGL, BuildOptions.None);
        }

        private static string getRootPath() {
            if (!PlayerPrefs.HasKey(PathKey))
                return "";
            return unifyDirectorySeparatorChars(PlayerPrefs.GetString(PathKey));
        }

        private static void setRootPath(string newRootPath) {
            PlayerPrefs.SetString(PathKey, unifyDirectorySeparatorChars(newRootPath));
        }
    }
#endif
}