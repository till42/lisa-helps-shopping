﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;
using System;
using System.IO;

namespace FA.Editor.Build
{
    class FA_ItchPostBuild : IPostprocessBuildWithReport
    {
        public int callbackOrder
        {
            get { return 0; }
        }

        [SerializeField] string zipName = "build.zip";

        [SerializeField] string[] folders =
        {
            "Build",
            "TemplateData",
        };

        private string getZipPath(string path)
        {
            Directory.CreateDirectory(path);
            return Path.Combine(path, zipName);
        }



        public void OnPostprocessBuild(BuildReport report)
        {
            Debug.Log(string.Format("Post prossing {0} Build at {1} with folders={2}, zipName={3}",
                report.summary.platform, report.summary.outputPath, folders, zipName));

            //only for WebGL Build
            if (report.summary.platform != BuildTarget.WebGL)
                return;

            //delete the previous output file
            if (File.Exists(Path.Combine(report.summary.outputPath, zipName)))
            {
                // If file found, delete it    
                File.Delete(Path.Combine(report.summary.outputPath, zipName));
            }


            //zip the two folders "Build" and "TemplateData" to "build.zip" here
            ZipUtil.ZipFoldersAndFiles(getZipPath(report.summary.outputPath), "index.html", folders);
        }
    }
}
#endif