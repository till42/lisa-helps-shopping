﻿#if KONG
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;
using System;
using System.IO;

class FA_KongPostBuild : IPostprocessBuildWithReport {
    public int callbackOrder { get { return 0; } }



    [SerializeField]
    string zipName = "game.zip";

    [SerializeField]
    string[] folders =
    {
        "Build",
        "TemplateData"
    };

    private string getZipPath(string path) {
        Directory.CreateDirectory(path);
        return Path.Combine(path, zipName);
    }



    public void OnPostprocessBuild(BuildReport report) {
        Debug.Log(string.Format("Post prossing {0} Build at {1} with folders={2}, zipName={3}", report.summary.platform, report.summary.outputPath, folders, zipName));

        //only for WebGL Build
        if (report.summary.platform != BuildTarget.WebGL)
            return;


        //zip the two folders "Build" and "TemplateData" to "game.zip" here
        ZipUtil.ZipFolders(getZipPath(report.summary.outputPath), folders);
    }
}
#endif
#endif