﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using UnityEditor;
using UnityEngine;

namespace FA.Editor
{
    public static class Version
    {
#if UNITY_EDITOR
        public static string get => PlayerSettings.bundleVersion;

        public static void Set(string version) => PlayerSettings.bundleVersion = version;
#endif
    }
}