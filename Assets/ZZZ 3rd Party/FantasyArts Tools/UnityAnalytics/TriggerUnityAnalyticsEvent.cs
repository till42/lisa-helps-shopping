﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

namespace FA.UnityAnalytics {
public abstract class TriggerUnityAnalyticsEvent : MonoBehaviour {
    [SerializeField] protected AnalyticsEventTracker _analyticsEventTracker;

    public virtual void DoTrigger(int value) {
        _analyticsEventTracker.TriggerEvent();
    }
}
}