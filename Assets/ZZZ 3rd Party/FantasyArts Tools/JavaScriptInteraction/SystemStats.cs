﻿using UnityEngine;

namespace FA.JavaScriptInteraction {
public class SystemStats : MonoBehaviour {
    public string GraphicsDeviceType {
        get {
            string returnString = SystemInfo.graphicsDeviceType.ToString();
#if !UNITY_EDITOR
                    Debug.Log($"GraphicsDeviceType={returnString}");
#endif
            return returnString;
        }
    }

    public string OperatingSystem {
        get {
            string returnString = SystemInfo.operatingSystem;
#if !UNITY_EDITOR
                    Debug.Log($"OperatingSystem={returnString}");
#endif
            return returnString;
        }
    }

#if UNITY_WEBGL && !UNITY_EDITOR
        [DllImport("__Internal")]
        private static extern string GetBrowser();
#else
    /// <summary>
    /// Sync the filesystem within HTML5. Only required in WebGL build
    /// </summary>
    /// <returns>0, if succesfull. Err code otherwise</returns>
    private static string GetBrowser() {
        return "Unity Editor";
    }
#endif
    public static string Browser {
        get {
            string returnString = GetBrowser();
#if !UNITY_EDITOR
                    Debug.Log($"Browser={returnString}");
#endif
            return returnString;
        }
    }
}
}