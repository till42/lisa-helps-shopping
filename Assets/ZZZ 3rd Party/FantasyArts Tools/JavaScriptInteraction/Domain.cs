﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

namespace FA.JavaScriptInteraction {
public class Domain : MonoBehaviour {
    /// <summary>
    /// the detected domain as string (e.g. for analytics output)
    /// </summary>
    public string DomainTypeString => DomainType.ToString();

    /// <summary>
    /// the detected domain type as retrieved from URL in the Awake
    /// </summary>
    public DomainType DomainType { get; private set; } = DomainType.NotDetected;

    /// <summary>
    /// the URL detected in Awake
    /// </summary>
    public string URL { get; private set; } = "";

#if UNITY_WEBGL && !UNITY_EDITOR
        [DllImport("__Internal")]
        private static extern string GetURLFromPage();
#else
    /// <summary>
    /// Sync the filesystem within HTML5. Only required in WebGL build
    /// </summary>
    /// <returns>0, if succesfull. Err code otherwise</returns>
    private static string GetURLFromPage() {
        return "www.dummy.com";
    }
#endif
#if UNITY_WEBGL && !UNITY_EDITOR
        [DllImport("__Internal")]
        private static extern string GetQueryParam(string paramId);
#else
    private static string GetQueryParam(string paramId) {
        return "";
    }
#endif

    void Awake() {
        URL = GetURLFromPage();
        DomainType = getDomainType(URL);
        Debug.Log($"Domain found={DomainType}");
    }

    public string ReadQueryParam(string paramId) {
        return GetQueryParam(paramId);
    }

    /// <summary>
    /// get the domain where the application is running on by parsing the url
    /// </summary>
    /// <returns>Domain enum: e.g. Kongregate</returns>
    private DomainType getDomainType(string url) {
        //check if we are in the Unity Editor
        if (Application.isEditor)
            return DomainType.UnityEditor;

        //if the string is null or empty -> could not detect
        if (string.IsNullOrEmpty(url))
            return DomainType.NotDetected;

        //check if running on localhost
        if (url.StartsWith("http://127.0.0.1") || url.StartsWith("https://127.0.0.1"))
            return DomainType.Localhost;

        //if running on file system, e.g."file:///C:/Projects/Cleo_BUILD/v0.30d/index.html"
        if (url.StartsWith("file:"))
            return DomainType.FileSystem;

        //if running on Kongregate, e.g. "https://www.kongregate.com/games/till42/cleo-beta_preview"
        if (url.StartsWith("https://www.kongregate.com") || url.Contains("www.kongregate.com")) {
            //recognize Kong preview page
            if (url.EndsWith("_preview"))
                return DomainType.KongregatePreview;
            //otherwise, it is the live environment
            return DomainType.KongregateLive;
        }

        //if running on itch.io, e.g. "https://jinxedbyte.itch.io/cleos-gold-beta"
        if (url.Contains("itch.io")) {
            var split = url.Split('.');
            if (split.Length > 2 && split[1] == "itch" && split[2] == "io")
                return DomainType.Itch;
        }

        //if running on newgrounds, e.g. "https://www.newgrounds.com/portal/view/751763"
        if (url.Contains("newgrounds"))
            return DomainType.Newgrounds;

        //if running on another site (arg!)
        if (url.StartsWith("http"))
            return DomainType.Other;

        //otherwise, we do not know
        return DomainType.NotDetected;
    }
}

public enum DomainType {
    NotDetected,
    UnityEditor,
    Localhost,
    FileSystem,
    KongregateLive,
    KongregatePreview,
    Itch,
    Newgrounds,
    Other,
}

public static class DomainTypeExtensions {
    public static bool IsKong(this DomainType domainType) =>
        domainType == DomainType.KongregateLive || domainType == DomainType.KongregatePreview;
}
}