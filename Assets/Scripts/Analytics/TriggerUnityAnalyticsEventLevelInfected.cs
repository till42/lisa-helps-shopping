﻿using System.Collections;
using System.Collections.Generic;
using Corona.Stage;
using FA.UnityAnalytics;
using UnityEngine;
using UnityEngine.Analytics;

namespace Corona.Analytics {
public class TriggerUnityAnalyticsEventLevelInfected : TriggerUnityAnalyticsEventLevelCompleted {
    [SerializeField] private StageManager _stageManager;

    public void DoTriggerCurrentLevel() {
        DoTrigger(_stageManager.LevelIndex);
    }
}
}