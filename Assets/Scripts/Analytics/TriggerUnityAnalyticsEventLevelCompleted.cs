﻿using System.Collections;
using System.Collections.Generic;
using Corona.Stage;
using FA.UnityAnalytics;
using UnityEngine;
using UnityEngine.Analytics;

namespace Corona.Analytics {
public class TriggerUnityAnalyticsEventLevelCompleted : MonoBehaviour {
    [SerializeField] protected string _eventName = "Level_Completed";

    [SerializeField] private StageDefinition _stageDefinition;

    public void DoTrigger(int value) {
        Dictionary<string, object> parameters
            = new Dictionary<string, object>();

        if (value < _stageDefinition.LevelDefinitions.Count)
            parameters.Add("levelname", _stageDefinition.LevelDefinitions[value].DisplayName);

        parameters.Add("levelIndex", value);

        AnalyticsResult result = AnalyticsEvent.Custom(_eventName, parameters);

        if (result == AnalyticsResult.Ok)
            Debug.Log($"succesfully submitted to UnityAnalytics: {_eventName} with parameters {parameters}");
        else
            Debug.Log($"failed to submit to UnityAnalytics: {_eventName} with parameters {parameters}");
    }
}
}