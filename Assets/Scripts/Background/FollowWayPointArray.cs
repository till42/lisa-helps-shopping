﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Corona.Background   {
    public class FollowWayPointArray : MonoBehaviour
        {
        public Vector3[] WayPoints;
        public float Duration;

        void Start()
            {
            WayPoints = new Vector3[12];
            WayPoints[0] = new Vector3(0, 0, 0);
            WayPoints[1] = new Vector3(5, 2, 0);
            WayPoints[2] = new Vector3(6, 0, 0);
            WayPoints[3] = new Vector3(7, 3, 0);
            WayPoints[0] = new Vector3(9, 2, 0);
            WayPoints[1] = new Vector3(10, 2, 0);
            WayPoints[2] = new Vector3(6, 1, 0);
            WayPoints[3] = new Vector3(4, 3.5f, 0);
            WayPoints[0] = new Vector3(2, -2, 0);
            WayPoints[1] = new Vector3(-4, -3, 0);
            WayPoints[2] = new Vector3(2, 1, 0);
            WayPoints[3] = new Vector3(0, 0, 0);

            Duration = 10.0f;

            StartPath();
            }

        void StartPath()
            {
            transform.DOLocalPath(WayPoints, Duration, PathType.CatmullRom, PathMode.Full3D, 10, Color.white)
                .SetEase(Ease.Linear)
                .OnWaypointChange(OnReachWayPoint);
            }

        void OnReachWayPoint(int i)
            {
            
            if (i == 12)
                {
                StartPath();
                }
            
            }
        }
    }
