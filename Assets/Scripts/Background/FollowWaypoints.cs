﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Corona.Background {
public class FollowWaypoints : MonoBehaviour {
    [SerializeField] private Vector3 _newPos;

    [SerializeField] private float _duration;

    void Start() {
        transform.DOMove(_newPos, _duration).SetEase(Ease.Linear).SetLoops(-1);
    }
}
}