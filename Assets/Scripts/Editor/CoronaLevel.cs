﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Reflection;
using UnityEditor.SceneManagement;
using UnityEngine.SceneManagement;
using Corona.Stage;
using FA.Architecture.Scriptable;


namespace Corona.Editor {
public class CoronaLevel : EditorWindow {
    private StageDefinition[] _stageDefinitions;

    [MenuItem("FantasyArts/Load Level...", priority = 0)]
    static void Load() {
        Display();
    }

    public static void Display() {
        CoronaLevel window = CreateInstance<CoronaLevel>();
        window.position = new Rect(Screen.width / 2, Screen.height / 2, 550, 550);
        window.titleContent = new GUIContent("Load Level");
        window.showModal();
    }

    void showModal() {
        _stageDefinitions = ScriptableObjectExtensions.GetAllInstances<StageDefinition>();

        MethodInfo dynShowModal =
            this.GetType().GetMethod("ShowModal", BindingFlags.NonPublic | BindingFlags.Instance);
        dynShowModal.Invoke(this, new object[] { });
    }


    void OnGUI() {
        GUILayout.Space(10);

        foreach (var stageDefinition in _stageDefinitions) {
            foreach (var levelDefinition in stageDefinition.LevelDefinitions) {
                if (GUILayout.Button(levelDefinition.SceneName)) {
                    LoadScene(levelDefinition.ScenePath);
                    Close();
                }
            }
        }

        // EditorGUILayout.LabelField("Current Version:", currentVersion, EditorStyles.wordWrappedLabel);
        // GUILayout.Space(10);
        // buildVersion = EditorGUILayout.TextField("Version: ", buildVersion);
        // GUILayout.Space(60);
        // outputPath = $"{rootPath}{Path.DirectorySeparatorChar}v{buildVersion}";
        // EditorGUILayout.LabelField("Output Path:", outputPath, EditorStyles.wordWrappedLabel);
        // if (GUILayout.Button("Change output root")) {
        //     string newRoot = EditorUtility.OpenFolderPanel("Select Build Output Root Path", rootPath, "");
        //     newRoot = unifyDirectorySeparatorChars(newRoot);
        //     if (!string.IsNullOrEmpty(newRoot))
        //         rootPath = newRoot;
        // }
        // GUILayout.Space(100);
        // EditorGUILayout.BeginHorizontal();
        // if (GUILayout.Button("Cancel")) {
        //     Close();
        // }
        // if (GUILayout.Button("BUILD")) {
        //     EditorGUILayout.EndHorizontal();
        //     //write version, date and time into the version prefab
        //     if (buildVersion != "")
        //         Version.Set(buildVersion);
        //
        //     //save the root path for next time
        //     setRootPath(rootPath);
        //
        //     //close the window
        //     Close();
        //
        //     //perform the build
        //     performBuild();
        //     return;
        // }
        // EditorGUILayout.EndHorizontal();
    }

    private const string ApplicationScene = "Assets/Scenes/application.unity";
    private const string GameScene = "Assets/Scenes/game.unity";
    private const string StageScene = "Assets/Scenes/stage1.unity";

    private void LoadScene(string levelName) {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene(ApplicationScene, OpenSceneMode.Single);
        EditorSceneManager.OpenScene(GameScene, OpenSceneMode.Additive);
        EditorSceneManager.OpenScene(StageScene, OpenSceneMode.Additive);
        EditorSceneManager.OpenScene(levelName, OpenSceneMode.Additive);
    }
}
}
#endif