﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;


public class CoronaMenu : MonoBehaviour {
    private const string ApplicationScene = "Assets/Scenes/application.unity";
    private const string MenuScene = "Assets/Scenes/menu.unity";
    private const string CreditsScene = "Assets/Scenes/credits.unity";
    private const string CongratsScene = "Assets/Scenes/congratulations.unity";

    [MenuItem("FantasyArts/Load menu scene", priority = 1)]
    static void LoadMenuScene() {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene(ApplicationScene, OpenSceneMode.Single);
        EditorSceneManager.OpenScene(MenuScene, OpenSceneMode.Additive);
    }

    [MenuItem("FantasyArts/Load credits scene", priority = 2)]
    static void LoadCreditScene() {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene(ApplicationScene, OpenSceneMode.Single);
        EditorSceneManager.OpenScene(CreditsScene, OpenSceneMode.Additive);
    }

    [MenuItem("FantasyArts/Load congrats scene", priority = 3)]
    static void LoadCongratsScene() {
        EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        EditorSceneManager.OpenScene(ApplicationScene, OpenSceneMode.Single);
        EditorSceneManager.OpenScene(CongratsScene, OpenSceneMode.Additive);
    }

    [MenuItem("FantasyArts/Reset PlayerPrefs", priority = 3)]
    static void ResetPlayerPrefs() {
        PlayerPrefs.DeleteAll();
    }
}

#endif