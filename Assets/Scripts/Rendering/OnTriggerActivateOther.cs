﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Corona.Rendering
{
    public class OnTriggerActivateOther : MonoBehaviour
    {

        [SerializeField] private GameObject _other;

        [SerializeField] private string _playerTag="Player";

        private void Awake()
        {
            _other.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(_playerTag))
            _other.SetActive(true);
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (other.CompareTag(_playerTag))
            _other.SetActive(false);
        }
    }
}