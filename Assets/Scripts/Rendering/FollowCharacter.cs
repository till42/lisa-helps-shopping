﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Corona.Rendering
{
    public class FollowCharacter : MonoBehaviour
    {
        private Transform _character = null;

        private Transform _transform;
        private Vector3 _defaultOffset;
        private Vector3 _initialPosition;

        private void Awake()
        {
            _transform = transform;
            _initialPosition = _transform.position;
        }

        public void Connect(Transform character)
        {
            _transform.position = _initialPosition;
            _character = character;
           _defaultOffset = _transform.position - _character.position;
        }
        
        void Update()
        {
            //only execute, if character is set
            if (_character == null)
                return;
            
            //update the paosition
            _transform.position = _character.position + _defaultOffset;
        }
    }
}