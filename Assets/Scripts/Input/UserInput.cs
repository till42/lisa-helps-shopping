﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Corona.Character {
public class UserInput : MonoBehaviour {
    public float Horizontal => _horizontal;
    private float _horizontal;

    [SerializeField] private float runSpeed = 40f;

    [SerializeField] private CharacterController2D _characterController2D;

    private void Awake() {
        _characterController2D = GetComponent<CharacterController2D>();
    }

    private void Update() {
        _horizontal = Input.GetAxisRaw(("Horizontal")) * runSpeed;
    }

    private void FixedUpdate() {
        // Debug.Log($"horizontal={_horizontal}");
        _characterController2D.Move(_horizontal * Time.fixedDeltaTime, false, false);
    }

    private void OnDisable() {
        _horizontal = 0f;
        _characterController2D.Move(_horizontal * Time.fixedDeltaTime, false, false);
    }
}
}