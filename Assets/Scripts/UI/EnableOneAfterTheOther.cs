﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Corona.UI {
public class EnableOneAfterTheOther : MonoBehaviour {
    [SerializeField] private float _duration = 0.1f;

    [SerializeField] private UnityEvent _onEnableItem;

    private Transform _transform;

    private void OnEnable() {
        StartCoroutine(Co_EnableOneByOne());
    }

    private IEnumerator Co_EnableOneByOne() {
        _transform = transform;
        for (int i = 0; i < _transform.childCount; i++) {
            var child = _transform.GetChild(i);
            child.gameObject.SetActive(true);
            _onEnableItem?.Invoke();
            yield return new WaitForSeconds(_duration);
        }
    }


    private void OnDisable() {
        _transform = transform;
        for (int i = 0; i < _transform.childCount; i++) {
            var child = _transform.GetChild(i);
            child.gameObject.SetActive(false);
        }
    }
}
}