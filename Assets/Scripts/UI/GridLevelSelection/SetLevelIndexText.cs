﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Corona.UI.GridLevelSelection {
public class SetLevelIndexText : GridLevel {
    private TMP_Text _tmpText;

    protected override void Awake() {
        _tmpText = GetComponent<TMP_Text>();
        base.Awake();
    }

    protected override void HandleUpdate() {
        if (_index > 0)
            _tmpText.text = _index.ToString();
        else
            _tmpText.text = "Intro";
    }
}
}