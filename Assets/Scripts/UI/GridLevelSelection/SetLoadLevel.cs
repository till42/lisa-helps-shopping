﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using Corona.Level;
using UnityEngine;
using UnityEngine.UI;

namespace Corona.UI.GridLevelSelection {
public class SetLoadLevel : GridLevel {
    public void DoSetLevel() {
        _stageManager.SetLevel(_index);
    }

    public void DoLoad() {
        _stageManager.PlayLevel();
    }

    protected override void HandleUpdate() {
    }
}
}