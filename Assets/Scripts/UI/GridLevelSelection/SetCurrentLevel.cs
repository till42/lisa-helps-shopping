﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using UnityEngine;
using UnityEngine.UI;

namespace Corona.UI.GridLevelSelection {
public class SetCurrentLevel : GridLevel {
    private Image _image;

    protected override void Awake() {
        _image = GetComponent<Image>();
        base.Awake();
    }

    protected override void HandleUpdate() {
        if (_index == _stageManager.LevelIndex)
            _image.enabled = true;
        else
            _image.enabled = false;
    }
}
}