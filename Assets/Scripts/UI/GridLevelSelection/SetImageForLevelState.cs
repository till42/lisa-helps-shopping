﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using Corona.Level;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace Corona.UI.GridLevelSelection {
public class SetImageForLevelState : GridLevel {
    [SerializeField] private LevelState _levelState;

    [SerializeField] private Sprite _sprite;

    private Image _image;

    protected override void Awake() {
        _image = GetComponent<Image>();
        base.Awake();
    }

    protected override void HandleUpdate() {
        if (LevelState == _levelState)
            _image.sprite = _sprite;
    }
}
}