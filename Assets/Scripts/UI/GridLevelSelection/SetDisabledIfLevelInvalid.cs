﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using Corona.Level;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Corona.UI.GridLevelSelection {
public class SetDisabledIfLevelInvalid : GridLevel {
    private Button _button;
    private Image _image;
    private TMP_Text _tmpText;

    protected override void Awake() {
        _button = GetComponent<Button>();
        _image = GetComponent<Image>();
        _tmpText = GetComponent<TMP_Text>();
        base.Awake();
    }

    protected override void HandleUpdate() {
        if (_button != null)
            _button.enabled = IsLevelValid;

        if (_image != null)
            _image.enabled = IsLevelValid;

        if (_tmpText != null)
            _tmpText.enabled = IsLevelValid;
    }
}
}