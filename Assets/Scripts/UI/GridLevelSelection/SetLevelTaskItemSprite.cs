﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Level;
using UnityEngine;
using UnityEngine.UI;

namespace Corona.UI.GridLevelSelection {
public class SetLevelTaskItemSprite : GridLevel {
    private Image _image;

    protected override void Awake() {
        _image = GetComponent<Image>();
        base.Awake();
    }

    protected override void HandleUpdate() {
        if (LevelState != LevelState.NoLevel && TaskItem != null)
            _image.sprite = TaskItem.Sprite;
    }
}
}