﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Items;
using Corona.Level;
using Corona.Stage;
using FA.UI.GridLayout;
using UnityEngine;

namespace Corona.UI.GridLevelSelection {
public abstract class GridLevel : MonoBehaviour {
    protected int _index;
    protected StageManager _stageManager;

    protected LevelState LevelState => _stageManager.StageProgress.GetState(_index);

    protected ItemDefinition TaskItem => _stageManager.StageDefinition.GetLevelDefinitionAt(_index)?.TaskItem;

    protected bool IsLevelValid => _stageManager.StageDefinition.GetLevelDefinitionAt(_index) != null;

    protected virtual void Awake() {
        var gridIndexCounter = GetComponentInParent<GridIndexCounter>();
        _index = gridIndexCounter.Index;
        _stageManager = FindObjectOfType<StageManager>();
    }

    protected void OnEnable() {
        HandleUpdate();
    }

    protected void Start() {
        HandleUpdate();
    }

    protected abstract void HandleUpdate();
}
}