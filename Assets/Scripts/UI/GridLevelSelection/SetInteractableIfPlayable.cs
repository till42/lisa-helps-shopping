﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using Corona.Level;
using UnityEngine;
using UnityEngine.UI;

namespace Corona.UI.GridLevelSelection {
public class SetInteractableIfPlayable : GridLevel {
    private Button _button;

    protected override void Awake() {
        _button = GetComponent<Button>();
        base.Awake();
    }

    protected override void HandleUpdate() {
        if (LevelState == LevelState.LevelCompleted || LevelState == LevelState.LevelUnlocked)
            _button.interactable = true;
        else
            _button.interactable = false;
    }
}
}