﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Architecture;
using Corona.Items;
using Corona.Level;
using Corona.Stage;
using NaughtyAttributes;
using UnityEngine;

namespace Corona.UI {
public class LevelButton : MonoBehaviour {
    public ItemDefinition ItemDefinition { get; private set; }

    public Action _onUpdate;

    public int DisplayIndex => _index + 1;
    private int _index;

    private StageManager _stageManager;
    private StageDefinition _stageDefinition;

    void OnEnable() {
        _index = GetLevelIndex();
        name = $"Level Button index={_index}";
        ItemDefinition = GetItemDefinition(_index);
        _onUpdate?.Invoke();
    }

    private int GetLevelIndex() {
        var panel = GetComponentInParent<PanelLevelButtonIndex>();
        return (panel.Row * panel.Max) + transform.GetSiblingIndex();
    }

    private ItemDefinition GetItemDefinition(int levelIndex) {
        if (levelIndex < 0 || _stageDefinition == null || levelIndex >= _stageDefinition.LevelDefinitions.Count)
            return null;
        return _stageDefinition.LevelDefinitions[levelIndex].TaskItem;
    }
}
}