﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices.WindowsRuntime;
using Corona.Level;
using Corona.Stage;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace Corona.UI {
public class ClearImageForLevelState : BindLevelButton {
    [SerializeField] private StageProgress _stageProgress;

    [SerializeField] private LevelState _levelState;

    private Image _image;

    private TMP_Text _tmpText;

    protected override void Awake() {
        _image = GetComponent<Image>();
        _tmpText = GetComponent<TMP_Text>();
        base.Awake();
    }

    protected override void HandleUpdate() {
        // if (_stageProgress.GetState(_levelButton.) == _levelState) {
        //     if (_image != null) _image.enabled = false;
        //     if (_tmpText != null) _tmpText.enabled = false;
        // }
        // else {
        //     if (_image != null) _image.enabled = true;
        //     if (_tmpText != null) _tmpText.enabled = true;
        // }
    }
}
}