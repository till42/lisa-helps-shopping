﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Corona.UI {
public class PanelLevelButtonIndex : MonoBehaviour {
    public int Row => transform.GetSiblingIndex();

    public int Max => transform.childCount;

    private void Awake() {
        name = $"Panel Level Buttons - Row {Row}";
    }
}
}