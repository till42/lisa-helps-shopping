﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Corona.UI {
public abstract class BindLevelButton : MonoBehaviour {
    [SerializeField] protected LevelButton _levelButton;

    protected virtual void Awake() {
        if (_levelButton == null) {
            Debug.LogWarning($"no _levelButton found", this);
            return;
        }
    }

    protected virtual void OnEnable() {
        Bind(_levelButton);
    }

    private void Bind(LevelButton levelButton) {
        levelButton._onUpdate += HandleUpdate;
        HandleUpdate();
    }

    protected abstract void HandleUpdate();

    private void UnBind(LevelButton levelButton) {
        levelButton._onUpdate -= HandleUpdate;
    }

    protected virtual void OnDestroy() {
        UnBind(_levelButton);
    }
}
}