﻿using Corona.Items;
using Corona.Level;
using Corona.UI;
using UnityEngine.Events;

namespace Corona.Architecture {
[System.Serializable]
public class UnityEventLevelButtonState : UnityEvent<LevelState> {
}
}