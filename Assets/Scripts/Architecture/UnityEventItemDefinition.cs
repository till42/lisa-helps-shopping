﻿using Corona.Items;
using UnityEngine.Events;

namespace Corona.Architecture {
[System.Serializable]
public class UnityEventItemDefinition : UnityEvent<ItemDefinition> {
}
}