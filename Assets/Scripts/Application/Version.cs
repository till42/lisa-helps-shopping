﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Corona.Architecture {
public class Version : MonoBehaviour {
    public string V => UnityEngine.Application.version;
}
}