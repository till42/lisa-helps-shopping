﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using FA.SceneManagement;

namespace Corona.Application {
public class LoadScenesAdditive : MonoBehaviour {
    [SerializeField] private List<string> sceneNames;

    [SerializeField] private bool _isLoadOnStartupInBuild = false;

    private void Start() {
        if (_isLoadOnStartupInBuild && SceneManager.sceneCount <= 1)
            DoLoadScenes();
    }

    public void DoLoadScenes() {
        StartCoroutine(Co_LoadScenes());
    }

    /// <summary>
    /// unload all scenes that are not the one of the gameobject (expected=application). Then Load the specified scenes
    /// </summary>
    /// <returns></returns>
    private IEnumerator Co_LoadScenes() {
        SceneManager.SetActiveScene(gameObject.scene);

        int count = SceneManager.sceneCount;

        //go through all scnenes... backwards
        for (int i = SceneManager.sceneCount - 1; i >= 0; i--) {
            var scene = SceneManager.GetSceneAt(i);
            if (scene == gameObject.scene)
                continue;

            //unload the scene
            var unloadSceneAsync = SceneManager.UnloadSceneAsync(scene);
            if (unloadSceneAsync == null) {
                Debug.LogWarning($"could not unload scene {scene.name}.");
            }

            //wait until unload completed
            while (unloadSceneAsync != null && !unloadSceneAsync.isDone)
                yield return null;
        }

        //after everything unloaded: load the scenes we need
        LoadScenes();
    }

    private void LoadScenes() {
        foreach (var sceneName in sceneNames)
            LoadSingleScene(sceneName);
    }

    private static void LoadSingleScene(string sceneName) {
        //do not load the scene, if already loaded
        if (SceneManagementExtensions.IsLoaded(sceneName))
            return;

        //otherwise load the scene additively
        SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
    }
}
}