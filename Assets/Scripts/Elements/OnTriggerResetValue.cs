﻿using System.Collections;
using System.Collections.Generic;
using FA.Architecture.Scriptable;
using UnityEngine;

namespace Corona.Elements {
public class OnTriggerResetValue : MonoBehaviour {
    [SerializeField] private ScriptableFloat _scriptableFloat;

    [SerializeField] private ScriptableBool _scriptableBool1;
    [SerializeField] private ScriptableBool _scriptableBool2;

    [SerializeField] private string _playerTag = "Player";

    private void OnTriggerEnter2D(Collider2D other) {
        if (!other.CompareTag(_playerTag))
            return;

        if (_scriptableFloat != null)
            _scriptableFloat.Value = 0;

        if (_scriptableBool1 != null)
            _scriptableBool1.Value = false;

        if (_scriptableBool2 != null)
            _scriptableBool2.Value = false;
    }
}
}