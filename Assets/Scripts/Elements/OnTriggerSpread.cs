﻿using System.Collections;
using System.Collections.Generic;
using FA.Architecture.Scriptable;
using UnityEngine;

namespace Corona.Elements
{
    public class OnTriggerSpread : MonoBehaviour
    {
    
        [SerializeField] private string playerTag="Player";

        [SerializeField] private ScriptableBool hasVirus;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            //not interested, if other is not the player
            if (!other.CompareTag(playerTag))
                return;

            hasVirus.Value = true;

        }
    }
}