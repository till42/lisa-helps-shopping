﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace Corona.Elements {
public class LoopMove : MonoBehaviour {
    [SerializeField] private float _speed = 2f;

    [SerializeField] private Transform _alternativeTransform;

    [SerializeField] private bool _isDebug = false;

    private Transform _transform;
    private Vector3 _defaultPosition;
    private Vector3 _alternateLocalPosition;
    private float _distance;
    private float _duration;

    private void Awake() {
        if (_isDebug)
            Debug.Log($"statement");

        _transform = transform;
        _defaultPosition = _transform.localPosition;
        _alternateLocalPosition = _alternativeTransform.localPosition + _defaultPosition;

        //calculate duration
        _distance = Vector3.Distance(_defaultPosition, _alternateLocalPosition);
        _duration = _distance / _speed;
    }

    private void Start() {
        _transform.DOLocalMove(_alternateLocalPosition, _duration).SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.Linear);
    }

    private void OnDisable() {
        _transform.DOKill();
    }
}
}