﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.UI;

namespace Corona.Items {
[RequireComponent(typeof(Image)), ExecuteInEditMode]
public class SetItemImage : MonoBehaviour {
    private Image _image;

    private Item _item;

    private ItemDefinition _itemDefinition;

    private void OnEnable() {
        Refresh();
    }

    [Button]
    private void Refresh() {
        _item = GetComponentInParent<Item>();
        if (_item == null)
            return;

        _itemDefinition = _item.ItemDefinition;
        _image = GetComponent<Image>();
        if (_image == null)
            return;
        _image.sprite = _itemDefinition.Sprite;
    }
}
}