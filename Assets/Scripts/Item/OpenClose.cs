﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace Corona.Items {
public class OpenClose : MonoBehaviour {
    [SerializeField] private GameObject Open;

    [SerializeField] private GameObject Closed;

    [SerializeField] private UnityEvent _onOpen;

    [SerializeField] private UnityEvent _onClose;

    public void Switch() {
        if (Open != null) {
            Open.SetActive(!Open.activeInHierarchy);
            if (Open.activeInHierarchy)
                _onOpen?.Invoke();
        }

        if (Closed != null) {
            Closed.SetActive(!Closed.activeInHierarchy);
            if (Closed.activeInHierarchy)
                _onClose?.Invoke();
        }
    }
}
}