﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Corona.Items {
public class SetAlternateTransform : MonoBehaviour {
    private Vector3 _defaultPosition;
    private Vector3 _defaultRotation;
    private Vector3 _defaultLocalScale;
    private int _defaultSortingOrder;

    private Transform _transform;
    private SpriteRenderer _spriteRenderer;

    private ItemDefinition _itemDefinition;

    private void Awake() {
        var item = GetComponentInParent<Item>();
        if (item == null) {
            Debug.LogWarning("Could not find item as expected", this);
            item = gameObject.AddComponent<Item>();
        }

        _spriteRenderer = GetComponent<SpriteRenderer>();
        if (_spriteRenderer == null) {
            Debug.LogWarning("Could not find spriterenderer as expected", this);
            _spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
        }

        _itemDefinition = item.ItemDefinition;

        _transform = transform;
        _defaultPosition = _transform.localPosition;
        _defaultRotation = _transform.localRotation.eulerAngles;
        _defaultLocalScale = _transform.localScale;
        _defaultSortingOrder = _spriteRenderer.sortingOrder;
    }

    public void ToAlternate() {
        if (_itemDefinition == null)
            return;

        if (_itemDefinition.AlternateLocalScale != Vector3.zero)
            _transform.localPosition = _itemDefinition.AlternatePosition;
        if (_itemDefinition.AlternateRotation != Vector3.zero)
            _transform.localRotation = Quaternion.Euler(_itemDefinition.AlternateRotation);
        if (_itemDefinition.AlternateLocalScale != Vector3.zero)
            _transform.localScale = _itemDefinition.AlternateLocalScale;
        if (_itemDefinition.AlternateSortingOrder != 0)
            _spriteRenderer.sortingOrder = _itemDefinition.AlternateSortingOrder;
    }

    public void ToDefault() {
        _transform.localPosition = _defaultPosition;
        _transform.localRotation = Quaternion.Euler(_defaultRotation);
        _transform.localScale = _defaultLocalScale;
        _spriteRenderer.sortingOrder = _defaultSortingOrder;
    }
}
}