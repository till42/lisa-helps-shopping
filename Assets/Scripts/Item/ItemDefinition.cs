﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace Corona.Items {
[CreateAssetMenu(fileName = "NewItem", menuName = "Corona/ItemDefinition")]
public class ItemDefinition : ScriptableObject {
    [ShowAssetPreview, SerializeField] private Sprite _sprite;
    public Sprite Sprite => _sprite;

    [SerializeField] private bool _isWashable = true;
    public bool IsWashable => _isWashable;

    [SerializeField] private bool _isPreventContamination = false;
    public bool IsPreventContamination => _isPreventContamination;

    [SerializeField] private bool _isUsageAnimation = false;
    public bool IsUsageAnimation => _isUsageAnimation;

    [SerializeField] private string _usageAnimationTrigger;
    public string UsageAnimationTrigger => _usageAnimationTrigger;

    [SerializeField] private Vector3 _alternatePosition;
    public Vector3 AlternatePosition => _alternatePosition;

    [SerializeField] private Vector3 _alternateRotation;
    public Vector3 AlternateRotation => _alternateRotation;

    [SerializeField] private Vector3 _alternateLocalScale;
    public Vector3 AlternateLocalScale => _alternateLocalScale;

    [SerializeField] private int _alternateSortingOrder;
    public int AlternateSortingOrder => _alternateSortingOrder;

    public override string ToString() {
        return Sprite.name;
    }
}
}