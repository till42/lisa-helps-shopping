﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Corona.Items {
public class RevertToDefaultContainer : MonoBehaviour {
    public void DoRevert() {
        var item = GetComponentInParent<Item>();
        item.DefaultItemContainer.ExchangeItem();
    }
}
}