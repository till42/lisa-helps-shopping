﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Character;
using UnityEngine;
using UnityEngine.Events;

namespace Corona.Items {
public class ItemContainer : MonoBehaviour {
    private Item _item;

    public void ExchangeItem() {
        var previousItem = GetComponentInChildren<Item>();
        var inventory = FindObjectOfType<Inventory>();

        //get the item from the inventory and place it here in the container
        var item = inventory.DropItem();
        _item = item;
        if (item != null)
            item.PutInto(this);

        //give the previous item to the inventory
        inventory.ReceiveItem(previousItem);
    }
}
}