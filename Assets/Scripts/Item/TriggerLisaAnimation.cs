﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Corona.Character;
using UnityEngine;
using UnityEngine.Events;

namespace Corona.Items {
public class TriggerLisaAnimation : MonoBehaviour {
    private Item _item;
    private ItemDefinition _itemDefinition;
    private AnimationEventsLisa _animationEventsLisa;

    [SerializeField] private UnityEvent _onCompleted;

    private void Awake() {
        _item = GetComponent<Item>();
        _itemDefinition = _item.ItemDefinition;
        _animationEventsLisa = FindObjectOfType<AnimationEventsLisa>();
    }

    public void DoUseItem() {
        _animationEventsLisa.UsageAnimation(_itemDefinition, () => { _onCompleted?.Invoke(); });
    }
}
}