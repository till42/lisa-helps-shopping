﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Character;
using UnityEngine;
using UnityEngine.Events;

namespace Corona.Items {
public class InteractActions : MonoBehaviour, IInteractable {
    [SerializeField] private UnityEvent _onPlayerInReach;

    [SerializeField] private UnityEvent _onPlayerOutOfReach;

    [SerializeField] private UnityEvent _onInteractStart;

    [SerializeField] private UnityEvent _onInteractEnd;

    private bool _isInteracting = false;

    private bool _isStopped = false;

    public void PlayerInReach() {
        if (_isStopped)
            return;
        _onPlayerInReach?.Invoke();
    }

    public void PlayerOutOfReach() {
        if (_isStopped)
            return;

        _onPlayerOutOfReach?.Invoke();

        if (_isInteracting)
            InteractEnd();
    }

    public void InteractStart() {
        if (_isStopped)
            return;
        _isInteracting = true;
        _onInteractStart?.Invoke();
    }

    public void InteractEnd() {
        if (_isStopped)
            return;
        _isInteracting = false;
        _onInteractEnd?.Invoke();
    }

    public void Stop() {
        _isStopped = true;
    }

    private void OnDisable() {
        _isStopped = false;
    }
}
}