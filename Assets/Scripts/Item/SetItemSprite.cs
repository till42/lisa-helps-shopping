﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

namespace Corona.Items {
[RequireComponent(typeof(SpriteRenderer)), ExecuteInEditMode]
public class SetItemSprite : MonoBehaviour {
    private SpriteRenderer _spriteRenderer;

    private Item _item;

    private ItemDefinition _itemDefinition;

    private void OnEnable() {
        _item = GetComponentInParent<Item>();
        _item.OnUpdate += Refresh;
        Refresh();
    }

    [Button]
    private void Refresh() {
        _itemDefinition = _item.ItemDefinition;
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _spriteRenderer.sprite = _itemDefinition.Sprite;
    }

    private void OnDestroy() {
        _item.OnUpdate -= Refresh;
    }
}
}