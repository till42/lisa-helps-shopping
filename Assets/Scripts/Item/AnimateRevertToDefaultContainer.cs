﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using DG.Tweening;


namespace Corona.Items {
public class AnimateRevertToDefaultContainer : MonoBehaviour {
    [SerializeField] public UnityEvent _onCompleted;

    [SerializeField] private Vector3 _axis = new Vector3(0f, 0f, 10f);

    [SerializeField] private float _duration = 2f;

    private Transform _transform;

    public void DoAnimate() {
        var item = GetComponentInParent<Item>();

        StartCoroutine(Co_Animation());
    }

    private IEnumerator Co_Animation() {
        _transform = transform;
        Vector3 defaultScale = _transform.localScale;
        Quaternion defaultLocalRotation = _transform.localRotation;
        _transform.localScale = _transform.localScale * 6f;

        _transform.DOScale(Vector3.zero, _duration);
        // _transform.DOLocalRotate(Vector3.zero, _duration / 4f, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Yoyo);


        var s = _transform.DOLocalRotate(_axis, 0.01f, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Incremental)
            .SetRelative(true);
        yield return new WaitForSeconds(_duration);
        _transform.DOKill();
        _transform.localScale = defaultScale;
        _transform.localRotation = defaultLocalRotation;
        _onCompleted?.Invoke();
    }
}
}