﻿using System.Collections;
using System.Collections.Generic;
using FA.Architecture.Scriptable;
using UnityEngine;

namespace Corona.Items {
public class CheckNotWashable : ScriptableEventListener {
    private ItemDefinition _itemDefinition;

    protected override void OnEnable() {
        base.OnEnable();
        var item = GetComponentInParent<Item>();
        if (item == null) {
            Debug.LogWarning($"no item found in parent");
            _itemDefinition = ScriptableObject.CreateInstance<ItemDefinition>();
            return;
        }

        _itemDefinition = item.ItemDefinition;
    }

    public override void OnEventRaised() {
        if (!_itemDefinition.IsWashable) {
            Debug.Log($"item being washed despite not washable");
            base.OnEventRaised();
        }
    }
}
}