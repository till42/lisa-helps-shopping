﻿using System;
using System.CodeDom;
using System.Collections;
using System.Collections.Generic;
using Corona.Character;
using FA.Animation;
using UnityEngine;
using NaughtyAttributes;
using UnityEngine.Events;

namespace Corona.Items {
public class Item : MonoBehaviour {
    [SerializeField] private ItemDefinition _itemDefinition;
    public ItemDefinition ItemDefinition => _itemDefinition;

    private Transform _transform;

    private Vector3 _defaultScale;

    public UnityEvent OnPicked;

    public UnityEvent OnPutAway;

    public ItemContainer DefaultItemContainer { get; private set; }

    public event Action OnUpdate = delegate { };

    private void OnValidate() {
        OnUpdate?.Invoke();
    }

    private void Awake() {
        _transform = transform;
        _defaultScale = _transform.localScale;
        DefaultItemContainer = GetComponentInParent<ItemContainer>();
        if (DefaultItemContainer == null) {
            Debug.LogWarning("item has no default container", this);
            DefaultItemContainer = gameObject.AddComponent<ItemContainer>();
        }
    }

    public void PutInto(Inventory inventory) {
        _transform.SetParent(inventory.transform, false);
        _transform.localScale = Vector3.one;
        OnPicked?.Invoke();
    }

    public void PutInto(ItemContainer itemContainer) {
        _transform.SetParent(itemContainer.transform, false);
        _transform.localScale = _defaultScale;
        OnPutAway?.Invoke();
    }

    public override string ToString() {
        if (_itemDefinition == null)
            return "null";
        return _itemDefinition.ToString();
    }
}
}