﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Corona.Character {
public interface IInteractable {
    void PlayerInReach();

    void PlayerOutOfReach();

    void InteractStart();

    void InteractEnd();
}
}