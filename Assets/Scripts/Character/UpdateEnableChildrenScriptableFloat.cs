﻿using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;
using FA.Architecture.Scriptable;

namespace FA.UI {
    public class UpdateEnableChildrenScriptableFloat : MonoBehaviour {

        [SerializeField] private ScriptableFloat scriptableFloat = default;

        protected void OnEnable() {
          
            scriptableFloat.OnValueChanged += HandleValueChanged;

            //update with default
            HandleValueChanged(scriptableFloat.Value);
        }

        protected virtual void HandleValueChanged(float value) {
         EnableChildrenUpTo(Mathf.CeilToInt(value));   
        }

        private void EnableChildrenUpTo(int count)
        {
            for (var i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                child.gameObject.SetActive(count>i);
            }
        }

        protected virtual void OnDisable() {
            scriptableFloat.OnValueChanged -= HandleValueChanged;
        }

    }
}