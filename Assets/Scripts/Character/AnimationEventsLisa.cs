﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Items;
using UnityEngine;
using UnityEngine.Events;

namespace Corona.Character {
public class AnimationEventsLisa : MonoBehaviour {
    [SerializeField] private Inventory _inventory;

    [SerializeField] private UnityEvent _onSprayEffect;

    private bool _isAnimationPlaying = false;

    private Animator _animator;

    private void Awake() {
        _animator = GetComponent<Animator>();
    }

    public void UsageAnimation(ItemDefinition itemDefinition, Action onCompleted) {
        if (!itemDefinition.IsUsageAnimation) {
            onCompleted?.Invoke();
            return;
        }

        StartCoroutine(Co_UsageAnimation(itemDefinition, onCompleted));
    }

    private IEnumerator Co_UsageAnimation(ItemDefinition itemDefinition, Action onCompleted) {
        _isAnimationPlaying = true;
        _animator.SetTrigger(itemDefinition.UsageAnimationTrigger);

        while (_isAnimationPlaying)
            yield return null;

        onCompleted?.Invoke();
    }

    public void TriggerAnimationSprayCompleted() {
        _isAnimationPlaying = false;
    }

    public void TriggerSprayingEffect() {
        _onSprayEffect?.Invoke();
    }

    public void TriggerAnimationGiveCarrotCompleted() {
        _isAnimationPlaying = false;
    }
}
}