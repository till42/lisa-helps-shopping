﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Corona.Character {
public class Lisa : MonoBehaviour {
    public Inventory Inventory { get; private set; }

    public AnimationEventsLisa AnimationEventsLisa { get; private set; }

    private void Awake() {
        Inventory = GetComponentInChildren<Inventory>();
        AnimationEventsLisa = GetComponentInChildren<AnimationEventsLisa>();
    }
}
}