﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Corona.Architecture;
using UnityEngine;
using Corona.Items;
using UnityEngine.Events;

namespace Corona.Character {
public class Inventory : MonoBehaviour {
    private List<Item> _items;

    private Lisa _lisa;

    private void Awake() {
        _lisa = GetComponentInParent<Lisa>();
        _items = new List<Item>();
    }

    public bool IsPreventContamination {
        get {
            if (_items == null || _items.Count <= 0)
                return false;
            foreach (var item in _items) {
                if (item.ItemDefinition.IsPreventContamination)
                    return true;
            }

            return false;
        }
    }

    public Item DropItem() {
        //get the first item in the inventory and remove it, if available
        var item = _items.FirstOrDefault();
        if (item != null)
            _items.Remove(item);
        return item;
    }

    public void UseItem(ItemDefinition itemDefinition, Action onCompleted) {
        if (!itemDefinition.IsUsageAnimation)
            return;
        _lisa.AnimationEventsLisa.UsageAnimation(itemDefinition, () => {
            ConsumeItemInInventory(itemDefinition);
            onCompleted?.Invoke();
        });
    }

    public void ConsumeItemInInventory(ItemDefinition itemDefinition) {
        var item = _items.Where(i => i.ItemDefinition == itemDefinition).FirstOrDefault();

        if (item == null)
            return;

        _items.Remove(item);

        Destroy(item.gameObject);
    }

    public void ReceiveItem(Item item) {
        if (item == null)
            return;

        _items.Add(item);
        item.PutInto(this);
    }


    /// <summary>
    /// check whether or not the inventory has items
    /// </summary>
    /// <returns>true: there are items in the inventory. false: no items in the inventory</returns>
    public bool HasItems() {
        if (_items == null || _items.Count <= 0)
            return false;
        return true;
    }

    /// <summary>
    /// check whether or not the inventory has an item with a given definition
    /// </summary>
    /// <returns>true: there are items in the inventory with the definition. false: no items in the inventory of the defintion</returns>
    public bool HasItemOfType(ItemDefinition itemDefinition) {
        //no items... no items of the type
        if (!HasItems())
            return false;

        //go through each item
        foreach (var item in _items) {
            if (item.ItemDefinition == itemDefinition)
                return true;
        }

        //nothing found: then no item of this type in the inventory
        return false;
    }
}
}