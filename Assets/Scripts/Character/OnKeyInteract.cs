﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Corona.Character {
public class OnKeyInteract : MonoBehaviour {
    [SerializeField] private string _interactableTag = "Interactable";

    [SerializeField] private string _interactAxis = "Jump";
    [SerializeField] private string _interactAxis2 = "Submit";
    
    // private IInteractable _interactable;

    private List<IInteractable> _currentlyColliding = new List<IInteractable>();

    private void OnTriggerEnter2D(Collider2D other) {
        //not interesting, if the other collider is not an interactable
        if (!other.CompareTag(_interactableTag))
            return;

        //get the interactable component, store it and tell it that the player is in reach
        var interactable = other.GetComponent<IInteractable>();

        //none found --> should not happen
        if (interactable == null) {
            Debug.LogWarning("collided with gameobject which was tagged interactable but has no interactable script");
            return;
        }

        //no other colliding interactable? --> then it is the one
        if (_currentlyColliding.Count == 0) {
            interactable.PlayerInReach();
        }

        //store all currently coolliding interactables
        if (!_currentlyColliding.Contains(interactable))
            _currentlyColliding.Add(interactable);

        // //if we already have an interactable in reach, then we are not in reach any more
        // _interactable?.PlayerOutOfReach();
        //
        // //store the new interactable and tell it that it is in reach now
        // _interactable = interactable;
    }

    private void OnTriggerExit2D(Collider2D other) {
        //not interesting, if the other collider is not an item
        if (!other.CompareTag(_interactableTag))
            return;

        //get the interactable
        var interactable = other.GetComponent<IInteractable>();

        if (interactable == null) {
            Debug.LogWarning("collided with gameobject which was tagged interactable but has no interactable script");
            return;
        }

        //remove the interactable from the list of currently colliding
        if (_currentlyColliding.Contains(interactable))
            _currentlyColliding.Remove(interactable);

        //not in reach any more, tell it
        interactable.PlayerOutOfReach();

        //check if there is another item in reach
        var otherInteractable = _currentlyColliding.FirstOrDefault();
        if (otherInteractable != null)
            otherInteractable.PlayerInReach();
    }

    private void Update() {
        //check for click of axis
        if ((Input.GetButtonDown(_interactAxis) || Input.GetButtonDown(_interactAxis2))&& _currentlyColliding.Count > 0) {
            //so, we have the interact button and are in reach of an interactable object, then perform the interaction
            _currentlyColliding[0]?.InteractStart();
            return;
        }

        if ((Input.GetButtonUp(_interactAxis) || Input.GetButtonUp(_interactAxis2) )&& _currentlyColliding.Count > 0) {
            //so, we have the interact button and are in reach of an interactable object, then perform the interaction
            _currentlyColliding[0]?.InteractEnd();
            return;
        }
    }
}
}