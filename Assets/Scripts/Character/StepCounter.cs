﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FA.Architecture.Scriptable;


namespace Corona.Character {
/// <summary>
/// count the number of steps/units moved, if character has the virus
/// </summary>
public class StepCounter : MonoBehaviour {
    [SerializeField] private ScriptableFloat scriptableSteps;

    [SerializeField] private ScriptableBool hasVirus;

    private float _previousX;

    private float _currentX;

    private float _walkedX;

    private Transform _transform;

    private void Awake() {
        _transform = transform;
        _previousX = _transform.position.x;
    }

    private void FixedUpdate() {
        _currentX = _transform.position.x;

        _walkedX = Mathf.Abs(_previousX - _currentX);

        if (_walkedX > 0f)
            IncreaseValue(_walkedX);

        _previousX = _currentX;
    }

    private void IncreaseValue(float deltaAmount) {
        if (hasVirus.Value)
            scriptableSteps.Value += deltaAmount;
    }
}
}