﻿using System.Collections;
using System.Collections.Generic;
using FA.Architecture.Scriptable;
using UnityEngine;

namespace Corona.Character {
public class StateMachineSprayFX : StateMachineBehaviour {
    [SerializeField] private ScriptableEvent _scriptableEventSprayConsumed;

    [SerializeField] private AudioClip _audioClip;

    private AudioSource _audioSource;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        _audioSource = animator.GetComponent<AudioSource>();
        _audioSource.PlayOneShot(_audioClip);
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        _scriptableEventSprayConsumed?.Raise();
    }
}
}