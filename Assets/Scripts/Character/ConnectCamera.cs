﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Rendering;
using UnityEngine;

namespace Corona.Character
{
    public class ConnectCamera : MonoBehaviour
    {
        private FollowCharacter _followCharacter;
        
        private void OnEnable()
        {
            _followCharacter = FindObjectOfType<FollowCharacter>();
            if (_followCharacter == null)
            {
                Debug.LogWarning($"could not find Follow Character");
                _followCharacter = Camera.main.gameObject.AddComponent<FollowCharacter>();
            }
            
            _followCharacter.Connect(transform);
        }

        
    }
}