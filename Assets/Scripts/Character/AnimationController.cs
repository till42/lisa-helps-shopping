﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Corona.Character {
public class AnimationController : MonoBehaviour {
    [SerializeField] private Animator _animator;

    [SerializeField] private UserInput _userInput;

    private float _currentHorizontal = 0f;

    private void Update() {
        _currentHorizontal = _userInput.Horizontal;

        _animator.SetFloat("AbsHorizontal", Mathf.Abs(_currentHorizontal));
    }
}
}