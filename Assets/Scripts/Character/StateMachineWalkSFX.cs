﻿using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using UnityEngine;


namespace Corona.Character {
public class StateMachineWalkSFX : StateMachineBehaviour {
    private AudioSource _audioSource;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        _audioSource = animator.GetComponent<AudioSource>();
        _audioSource.Play();
    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        _audioSource.Stop();
    }
}
}