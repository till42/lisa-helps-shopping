﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Character;
using Corona.Level;
using Corona.Stage;
using FA.Architecture.Scriptable;
using UnityEngine;

namespace Corona.Game {
public class LevelWinCondition : MonoBehaviour {
    [SerializeField] private ScriptableContamination _scriptableContamination;

    [SerializeField] private ScriptableBool _isHandsDirty;

    private LevelDefinition _levelDefinition;
#if UNITY_EDITOR
    public LevelDefinition LevelDefinition => GetComponentInParent<LevelManager>().LevelDefinition;
    private Inventory _inventory => FindObjectOfType<Inventory>();
#else
    public LevelDefinition LevelDefinition => _levelDefinition;
    private Inventory _inventory;
#endif
    private LevelManager _levelManager;

    private void Awake() {
        _levelManager = GetComponentInParent<LevelManager>();
        _levelDefinition = _levelManager.LevelDefinition;
#if !UNITY_EDITOR
        _inventory = FindObjectOfType<Inventory>();
#endif
    }


    public bool CheckWinCondition() => IsItemConditionFulfilled()
                                       &&
                                       IsHandWashed();

    public bool IsItemConditionFulfilled() =>
        _inventory.HasItemOfType(LevelDefinition.TaskItem);

    public bool IsHandWashed() {
        if (LevelDefinition.IsTaskWashHands)
            return !_scriptableContamination.IsContaminated() && !_isHandsDirty.Value;
        else
            return true;
    }
}
}