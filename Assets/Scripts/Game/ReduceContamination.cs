﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Corona.Game {
public class ReduceContamination : MonoBehaviour {
    [SerializeField] private ScriptableContamination _scriptableContamination;

    [SerializeField] private float _tickLength = 0.5f;

    [SerializeField] private float _tickAmount = 0.2f;

    [SerializeField] private UnityEvent _onCompleted;

    public void Begin() {
        StopAllCoroutines();
        StartCoroutine(Co_Reduce());
    }

    public void End() {
        StopAllCoroutines();
    }

    private IEnumerator Co_Reduce() {
        var waitTick = new WaitForSeconds(_tickLength);

        while (_scriptableContamination.Value > 0) {
            yield return waitTick;
            _scriptableContamination.Value -= _tickAmount;
            if (_scriptableContamination.Value < 0f)
                _scriptableContamination.Value = 0f;
        }

        _onCompleted?.Invoke();
    }
}
}