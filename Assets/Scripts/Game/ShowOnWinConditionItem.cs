﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Corona.Game {
#if UNITY_EDITOR
[ExecuteInEditMode]
#endif
public class ShowOnWinConditionItem : MonoBehaviour {
    private LevelWinCondition _levelWinCondition;

    [Header("Item")] //---
    [SerializeField]
    private Image _imageItem;

    [SerializeField] Image _imageItemCheckmark;

    [Header("WashHandsCondition")] [SerializeField]
    private Image _imageHandsWashed;

    [SerializeField] private Image _imageHandsWashedCheckmark;


    private void OnEnable() {
        Refresh();
    }

    private void Refresh() {
        _levelWinCondition = FindObjectOfType<LevelWinCondition>();
        if (_levelWinCondition == null)
            return;
        _imageItem.sprite = _levelWinCondition.LevelDefinition.TaskItem.Sprite;
        _imageItemCheckmark.enabled = _levelWinCondition.IsItemConditionFulfilled();
        _imageHandsWashed.gameObject.SetActive(_levelWinCondition.LevelDefinition.IsTaskWashHands);
        _imageHandsWashedCheckmark.enabled = _levelWinCondition.IsHandWashed();
    }
}
}