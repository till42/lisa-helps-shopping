﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using System;
using FA.Architecture.Scriptable;
using UnityEngine.Events;

namespace Corona.Game {
public class CheatUnlockLevels : MonoBehaviour {
    [ReorderableList, SerializeField] private KeyCode[] _cheatCodes = default;
    private int _index;

    [SerializeField] private ScriptableBool _isCheatActivated;

    [SerializeField] private UnityEvent _onCheatActivated;

    void Start() {
        _index = 0;

#if UNITY_EDITOR
        // activate();
#endif
    }

    void Update() {
        // Check if any key is pressed
        if (!UnityEngine.Input.anyKeyDown)
            return;

        if (_index >= _cheatCodes.Length)
            _index = 0;


        // Check if the next key in the code is pressed
        if (UnityEngine.Input.GetKeyDown(_cheatCodes[_index])) {
            // Add 1 to index to check the next key in the code
            _index++;
        }
        // Wrong key entered, we reset code typing
        else {
            _index = 0;
        }

        // If index reaches the length of the cheatCode string, 
        // the entire code was correctly entered
        if (_index == _cheatCodes.Length) {
            activate();
        }
    }

    private void activate() {
        _isCheatActivated.Value = true;
        // Cheat code successfully inputted!
        _onCheatActivated?.Invoke();
        Debug.Log($"Cheats activated");
    }
}
}