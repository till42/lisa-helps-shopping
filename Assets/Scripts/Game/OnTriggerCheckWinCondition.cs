﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace Corona.Game
{
    public class OnTriggerCheckWinCondition : MonoBehaviour
    {
        [SerializeField] private GameObject _notWonGameObject;
        
        [SerializeField] private GameObject _wonGameObject;

        [SerializeField] private string _playerTag="Player";

        private bool _isWon { get; set; } = false;

        private LevelWinCondition _levelWinCondition;
        
        private void Awake()
        {
            _levelWinCondition = FindObjectOfType<LevelWinCondition>();
            _notWonGameObject.SetActive(false);
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.CompareTag(_playerTag))
                _notWonGameObject.SetActive(true);
            else
                return;
            
            if (CheckWinCondition())
            {
                _wonGameObject.SetActive(true);
                _isWon = true;
                return;
            }
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            if (_isWon)
                return;
            
            if (other.CompareTag(_playerTag))
                _notWonGameObject.SetActive(false);
        }

        private bool CheckWinCondition() => _levelWinCondition.CheckWinCondition();

    }
}