﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Character;
using Corona.Items;
using FA.Architecture.Scriptable;
using UnityEngine;
using UnityEngine.Events;

namespace Corona.Game {
public class TriggerSprayAction : MonoBehaviour {
    [SerializeField] private ScriptableEvent _onNothing;

    public void Nothing() {
        _onNothing?.Raise();
    }
}
}