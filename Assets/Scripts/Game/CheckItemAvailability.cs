﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Character;
using Corona.Items;
using UnityEngine;
using UnityEngine.Events;

namespace Corona.Game {
public class CheckItemAvailability : MonoBehaviour {
    [SerializeField] private ItemDefinition _itemDefinition;

    [SerializeField] private UnityEvent _onItemAvailable;

    [SerializeField] private UnityEvent _onItemUsed;

    private Inventory _inventory;

    private void Awake() {
        _inventory = FindObjectOfType<Inventory>();
        if (_inventory == null)
            Debug.LogWarning("No inventory found", this);
    }

    public void DoCheckForItem() {
        if (!IsItemInInventory()) return;

        _onItemAvailable?.Invoke();
    }

    public void DoItemAction() {
        if (!IsItemInInventory()) return;
        _inventory.UseItem(_itemDefinition, () => { _onItemUsed?.Invoke(); });
    }

    private bool IsItemInInventory() {
        if (_inventory == null)
            Debug.LogWarning("No inventory found", this);
        else if (!_inventory.HasItemOfType(_itemDefinition))
            return false;
        return true;
    }
}
}