﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Character;
using UnityEngine;
using UnityEngine.Events;

namespace Corona.Game {
public class CheckContamination : MonoBehaviour {
    private Inventory _inventory;

    [SerializeField] private UnityEvent _onContamination;

    private void Awake() {
        _inventory = FindObjectOfType<Inventory>();
        if (_inventory == null)
            Debug.LogWarning("No inventory found", this);
    }

    public void DoContamine() {
        if (gameObject == null)
            return;

        if (_inventory == null)
            Debug.LogWarning("No inventory found", this);
        else if (_inventory.IsPreventContamination)
            return;

        _onContamination?.Invoke();
    }
}
}