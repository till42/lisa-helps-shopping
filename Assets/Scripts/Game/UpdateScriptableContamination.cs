﻿using System.Collections;
using System.Collections.Generic;
using FA.UI;
using UnityEngine;

namespace Corona.Game {
public class UpdateScriptableContamination : UpdateScriptableFloat {
    protected override void HandleValueChanged(float value) {
        var scriptableContamination = (ScriptableContamination) _scriptableFloat;
        _onUpdate?.Invoke(scriptableContamination.PercentContaminated);
    }
}
}