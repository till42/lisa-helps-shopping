﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FA.Architecture.Scriptable;
using UnityEngine.Serialization;

namespace Corona.Game {
[CreateAssetMenu(menuName = "Corona/ScriptableContamination")]
public class ScriptableContamination : ScriptableFloat {
    [SerializeField] private float infectionThreshold = 20f;

    [SerializeField] private ScriptableEvent onInfectedEvent;

    public event Action OnInfected = delegate { };

    public bool IsInfectedCheck() => Value >= infectionThreshold;

    public bool IsContaminated() => Value > 0f;

    public float PercentContaminated => Value / infectionThreshold;

    public override void Reset() {
        base.Reset();
    }

    protected override void fireOnValueChanged() {
        if (IsInfectedCheck()) {
            onInfectedEvent.Raise();
        }

        base.fireOnValueChanged();
    }
}
}