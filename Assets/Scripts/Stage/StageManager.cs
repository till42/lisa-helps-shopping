﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Level;
using Corona.UI;
using FA.Architecture.Scriptable;
using UnityEngine;
using UnityEngine.SceneManagement;
using FA.SceneManagement;
using NaughtyAttributes;

namespace Corona.Stage {
public class StageManager : MonoBehaviour {
    [SerializeField] private StageDefinition _stageDefinition;
    public StageDefinition StageDefinition => _stageDefinition;

    [SerializeField] private StageProgress _stageProgress;
    public StageProgress StageProgress => _stageProgress;

    [SerializeField] private ScriptableEvent _stageCompleted;

    [SerializeField] private ScriptableInt _levelIndex;

    [SerializeField] private List<LevelState> _levelStates;
    public List<LevelState> LevelStates => _levelStates;

    public int LevelIndex => _levelIndex.Value;

    private void Start() {
        _levelIndex.Value = _stageProgress.GetHighestIndex();

#if !UNITY_EDITOR
        var levelName = _stageDefinition.GetSceneName(_levelIndex.Value);
        DoLoadLevel(levelName);
#else
        //check if any level is loaded
        int index = GetLoadedLevelIndex();
        if (index == -1 || index == 0) {
            var levelName = _stageDefinition.GetSceneName(_levelIndex.Value);
            DoLoadLevel(levelName);
        }
        else {
            //otherwise, get the level index and set it
            _levelIndex.Value = index;
        }
#endif
    }

    /// <summary>
    /// get the index of the level already loaded
    /// </summary>
    /// <returns>-1: if none found</returns>
    private int GetLoadedLevelIndex() {
        // go through each leveldefinition
        for (int i = 0; i < _stageDefinition.LevelDefinitions.Count; i++) {
            var levelDefinition = _stageDefinition.GetLevelDefinitionAt(i);
            //check if it is loaded
            if (SceneManagementExtensions.IsLoaded(levelDefinition.SceneName))
                return i;
        }

        //none found, return -1
        return -1;
    }

    [Button]
    public void PlayLevel() {
        var levelName = _stageDefinition.GetSceneName(_levelIndex.Value);
        DoLoadLevel(levelName);
    }

    public void NextLevel() {
        _levelIndex.Value++;


        var levelName = _stageDefinition.GetSceneName(_levelIndex.Value);
        if (string.IsNullOrEmpty(levelName)) {
            //no level -> end of stage
            _stageCompleted.Raise();
            return;
        }
    }

    public void SetLevel(int index) => _levelIndex.Value = index;

    public void DoLoadLevel(string levelName) {
        StartCoroutine(Co_LoadLevel(levelName));
    }

    private IEnumerator Co_LoadLevel(string levelName) {
        yield return Co_UnloadOtherLevels();

        //if  the level is empty
        if (string.IsNullOrEmpty(levelName)) {
            yield break;
        }

        //now load the level
        SceneManager.LoadScene(levelName, LoadSceneMode.Additive);
    }

    private IEnumerator Co_UnloadOtherLevels() {
        //if loaded, any level of this stage first
        for (int i = 0; i < SceneManager.sceneCount; i++) {
            var loadedScene = SceneManager.GetSceneAt(i);
            if (_stageDefinition.SceneNames.Contains(loadedScene.name)) {
                var unloadOperation = SceneManager.UnloadSceneAsync(loadedScene.name);
                if (unloadOperation != null) {
                    //wait until unload is done
                    while (!unloadOperation.isDone) {
                        yield return null;
                    }
                }
            }
        }
    }
}
}