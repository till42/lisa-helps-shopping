﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Level;
using Corona.UI;
using FA.SceneManagement;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Corona.Stage {
/// <summary>
/// runtime class to save the progress of each level
/// </summary>
[CreateAssetMenu(menuName = "Corona/StageProgress")]
public class StageProgress : ScriptableObject {
    [SerializeField] private List<LevelState> _levelStates;

    public event Action OnUpdate = delegate { };

    private const string PlayerPrefsKey = "STAGE_PROGRESS";
    private const string PlayerPrefsKeyCount = PlayerPrefsKey + "_COUNT";
    private string PlayerPrefsKeyForItem(int index) => PlayerPrefsKey + "_" + index;

    public void Reset(bool isLoadFromPlayerPrefs = true) {
        if (isLoadFromPlayerPrefs)
            _levelStates = Load();
        else
            _levelStates = null;

        if (_levelStates == null) {
            _levelStates = new List<LevelState>();
            _levelStates.Add(LevelState.LevelUnlocked);
        }
    }

    public int GetHighestIndex() {
        for (int i = _levelStates.Count - 1; i >= 0; i--) {
            if (_levelStates[i] == LevelState.LevelCompleted || _levelStates[i] == LevelState.LevelUnlocked)
                return i;
        }

        return 0;
    }

    public void Complete(int index) {
        CreateIndex(index);
        _levelStates[index] = LevelState.LevelCompleted;
        OnUpdate?.Invoke();
        Save();
    }

    public void Unlock(int index) {
        CreateIndex(index);

        if (_levelStates[index] != LevelState.LevelLocked)
            return;

        _levelStates[index] = LevelState.LevelUnlocked;
        OnUpdate?.Invoke();
        Save();
    }

    public void UnlockAll() {
        for (int i = 0; i < _levelStates.Count; i++)
            _levelStates[i] = LevelState.LevelUnlocked;
        OnUpdate?.Invoke();
    }

    public LevelState GetState(int index) {
        CreateIndex(index);
        return _levelStates[index];
    }

    private void SetState(int index, LevelState levelState) {
        CreateIndex(index);
        _levelStates[index] = levelState;
    }

    private void CreateIndex(int index) {
        if (_levelStates == null)
            Reset();

        if (index < 0 || index > 100) {
            Debug.LogWarning($"index (={index}) < 0 and >100 not defined");
            return;
        }

        if (_levelStates.Count > index)
            return;

        while (_levelStates.Count <= index) {
            _levelStates.Add(LevelState.LevelLocked);
        }
    }

    private void Save() {
        PlayerPrefs.SetInt(PlayerPrefsKeyCount, _levelStates.Count);
        for (int i = 0; i < _levelStates.Count; i++)
            PlayerPrefs.SetInt(PlayerPrefsKeyForItem(i), (int) _levelStates[i]);
    }

    public void ClearSave() {
        //check the highest count
        int count = 0;
        if (_levelStates != null)
            count = _levelStates.Count;
        var load = Load();
        if (load != null && load.Count > count)
            count = load.Count;

        //delete the count on player prefs
        if (PlayerPrefs.HasKey(PlayerPrefsKeyCount))
            PlayerPrefs.DeleteKey(PlayerPrefsKeyCount);

        //go through each element up to count to delete the player prefs
        for (int i = 0; i < count; i++) {
            PlayerPrefs.DeleteKey(PlayerPrefsKeyForItem(i));
        }
    }

    public List<LevelState> Load() {
        if (!PlayerPrefs.HasKey(PlayerPrefsKeyCount))
            return null;

        var output = new List<LevelState>();
        int count = PlayerPrefs.GetInt(PlayerPrefsKeyCount);
        for (int i = 0; i < count; i++) {
            LevelState levelState = (LevelState) PlayerPrefs.GetInt(PlayerPrefsKeyForItem(i));
            output.Add(levelState);
        }

        return output;
    }
}

/*
PlayerPrefs.SetInt("GameType", (int)GameType.SoloCompetitive);
And then you should cast back:

GameType gameType = PlayerPrefs.GetInt("GameType") as GameType;*/
}