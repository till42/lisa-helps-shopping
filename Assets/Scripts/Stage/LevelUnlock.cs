﻿using System;
using System.Collections;
using System.Collections.Generic;
using FA.Architecture.Scriptable;
using FA.Architecture.UnityEvents;
using UnityEngine;
using UnityEngine.Events;

namespace Corona.Stage {
public class LevelUnlock : MonoBehaviour {
    private StageManager _stageManager;

    [SerializeField] private UnityEventInt _onLevelComplete;

    [SerializeField] private ScriptableBool _isCheatActivated;

    private void Awake() {
        _stageManager = FindObjectOfType<StageManager>();
    }

    public void DoUnlockNext() {
        if (_isCheatActivated.Value)
            return;
        _stageManager.StageProgress.Unlock(_stageManager.LevelIndex + 1);
    }

    public void DoCompleteCurrent() {
        if (_isCheatActivated.Value)
            return;
        _stageManager.StageProgress.Complete(_stageManager.LevelIndex);
        _onLevelComplete?.Invoke(_stageManager.LevelIndex);
    }
}
}