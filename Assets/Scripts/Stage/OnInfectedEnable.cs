﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Game;
using UnityEngine;

namespace Corona.Stage
{
    public class OnInfectedEnable : MonoBehaviour
    {
        [SerializeField] private ScriptableContamination scriptableContamination;

        [SerializeField] private GameObject infectedCanvas;
        
        private void OnEnable()
        {
            scriptableContamination.OnInfected += HandleInfected;
            if (infectedCanvas==null)
                Debug.LogWarning($"no canvas found", context: this);
            infectedCanvas.SetActive(false);
        }

        private void HandleInfected()
        {
            infectedCanvas.SetActive(true);
        }
        
        private void OnDisable()
        {
            scriptableContamination.OnInfected -=HandleInfected;
        }
    }
}