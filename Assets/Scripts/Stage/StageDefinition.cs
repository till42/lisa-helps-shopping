﻿using System;
using System.Collections;
using System.Collections.Generic;
using FA.SceneManagement;
using NaughtyAttributes;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Corona.Stage {
[CreateAssetMenu(menuName = "Corona/StageDefinition")]
public class StageDefinition : ScriptableObject {
    [SerializeField] private int _index = 0;
    public int Index => _index;

    [SerializeField, ReorderableList] private List<LevelDefinition> _levelDefinitions;
    public List<LevelDefinition> LevelDefinitions => _levelDefinitions;

    public LevelDefinition GetLevelDefinitionAt(int index) {
        if (index < 0 || index >= LevelDefinitions.Count) return null;
        return LevelDefinitions[index];
    }

    public List<string> SceneNames {
        get {
            var output = new List<string>();
            if (_levelDefinitions == null || _levelDefinitions.Count <= 0)
                return output;
            foreach (var level in _levelDefinitions)
                output.Add(level.SceneName);
            return output;
        }
    }

    public string GetSceneName(int index) {
        if (_levelDefinitions == null || _levelDefinitions.Count <= index)
            return default;

        return _levelDefinitions[index].SceneName;
    }

    public static LevelDefinition GetLevelDefinition(Scene scene) {
        var stageDefinitions = Resources.FindObjectsOfTypeAll<StageDefinition>();
        foreach (var stageDefinition in stageDefinitions) {
            foreach (var levelDefinition in stageDefinition.LevelDefinitions) {
                if (levelDefinition.SceneName == scene.name)
                    return levelDefinition;
            }
        }

        return null;
    }

    public override string ToString() => "STAGE" + _index;
}
}