﻿using System;
using System.Collections;
using System.Collections.Generic;
using Corona.Stage;
using NaughtyAttributes;
using UnityEngine;

namespace Corona.Level {
[ExecuteInEditMode]
public class LevelManager : MonoBehaviour {
    [Label("Please change the level definition only in stageDefinition")]
    //----
    [SerializeField, DisableIf("true")]
    private LevelDefinition _levelDefinition;

    public LevelDefinition LevelDefinition => _levelDefinition;

    private void OnEnable() {
        var levelDefinition = StageDefinition.GetLevelDefinition(gameObject.scene);
        _levelDefinition = levelDefinition;
    }
}
}