﻿using System.Collections;
using System.Collections.Generic;
using Corona.Items;
using UnityEngine;

namespace Corona.Level
{
    [CreateAssetMenu(menuName = "Corona/ScriptableWinCondition")]
    public class ScriptableWinCondition : ScriptableObject
    {
        [SerializeField] private ItemDefinition _itemDefinition;
        
          public ItemDefinition itemDefinition => _itemDefinition;

          [SerializeField] private bool _isWashHands =true;
          public bool IsWashHands => _isWashHands;
    }
}