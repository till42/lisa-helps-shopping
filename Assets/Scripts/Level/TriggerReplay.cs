﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using Corona.Stage;
using UnityEngine;


namespace Corona.Level {
public class TriggerReplay : MonoBehaviour {
    private StageManager _stageManager;

    public void DoTrigger() {
        if (_stageManager == null)
            _stageManager = FindObjectOfType<StageManager>();
        _stageManager.PlayLevel();
    }
}
}