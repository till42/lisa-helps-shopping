﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Corona.Items;
using FA.SceneManagement;


[Serializable]
public class LevelDefinition {
    [SerializeField] private string _displayName;
    public string DisplayName => _displayName;

    [SerializeField] private SceneReference _sceneReference;
    public SceneReference SceneReference => _sceneReference;
    public string SceneName => _sceneReference.SceneName;
    public string ScenePath => _sceneReference.ScenePath;

    [SerializeField] private ItemDefinition _taskItem;
    public ItemDefinition TaskItem => _taskItem;

    [SerializeField] private bool _isTaskWashHands;
    public bool IsTaskWashHands => _isTaskWashHands;
}