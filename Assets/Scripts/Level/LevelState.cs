﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Corona.Level {
public enum LevelState {
    NoLevel = 0,
    LevelLocked = 1,
    LevelUnlocked = 2,
    LevelCompleted = 3,
}
}