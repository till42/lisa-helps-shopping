﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;

namespace Corona.Audio {
public class MuteAudioMixer : MonoBehaviour {
    [SerializeField] private AudioMixer _audioMixer;

    [SerializeField] private UnityEvent _onMuted;

    [SerializeField] private UnityEvent _onUnmuted;

    private const string volume = "Volume";
    private const float muted = -80f;
    private const float unmuted = 0f;

    public void DoSwitchMute() {
        float current = float.MinValue;
        _audioMixer.GetFloat(volume, out current);
        if (current > muted) {
            _audioMixer.SetFloat(volume, muted);
            _onMuted?.Invoke();
        }
        else {
            _audioMixer.SetFloat(volume, unmuted);
            _onUnmuted?.Invoke();
        }
    }
}
}